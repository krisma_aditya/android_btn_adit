package com.danareksa.investasik.util;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Handler;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

/**
 * Created by NEXTGEN on 5/17/2017.
 */

public class Animator {
    private static Handler myHandler = new Handler();

    public static void moveAnimationToShow(final View view) {
        TranslateAnimation animation = new TranslateAnimation(0, 0, 100, 0);
        animation.setDuration(100);
        animation.setFillAfter(false);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (view.getVisibility() == View.GONE) {
                    view.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        view.setAnimation(animation);
    }


    public static void moveAnimationToHide(final View view) {
        TranslateAnimation animation = new TranslateAnimation(0, 0, 0, 100);
        animation.setDuration(100);
        animation.setFillAfter(false);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (view.getVisibility() == View.VISIBLE) {
                    view.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        view.setAnimation(animation);
    }

    public void disappearView(final View view) {
        ObjectAnimator alpha = ObjectAnimator.ofFloat(view, "alpha", 0.0f);
        alpha.setDuration(400);

        alpha.start();

        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.GONE);
            }
        }, 400);
    }

    public void appearView(final View view) {
        ObjectAnimator alpha = ObjectAnimator.ofFloat(view, "alpha", 1.0f);
        alpha.setDuration(400);

        alpha.start();

        myHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.VISIBLE);
            }
        }, 400);
    }

    public static void appearView(View view, Integer duration) {
        view.setVisibility(View.VISIBLE);

        ObjectAnimator alpha = ObjectAnimator.ofFloat(view, "alpha", 1.0f);
        alpha.setDuration(duration);

        alpha.start();
    }


    public static void rotateView(View view, Float fromDegree, Float toDegree) {
        ObjectAnimator rotate = ObjectAnimator.ofFloat(view, "rotation", fromDegree, toDegree);
        rotate.setDuration(400);

        rotate.start();
    }


    public static void translatePopFromTop(View view) {
        view.setVisibility(View.VISIBLE);

        view.setY((float) -view.getHeight());

        ObjectAnimator translatePopFromTopp40 = ObjectAnimator.ofFloat(view, "translationY", 40);
        translatePopFromTopp40.setDuration(200);

        ObjectAnimator translatePopFromTopm30 = ObjectAnimator.ofFloat(view, "translationY", -30);
        translatePopFromTopm30.setDuration(200);

        ObjectAnimator translatePopFromTopp20 = ObjectAnimator.ofFloat(view, "translationY", 20);
        translatePopFromTopp20.setDuration(100);

        ObjectAnimator translatePopFromTopm10 = ObjectAnimator.ofFloat(view, "translationY", -10);
        translatePopFromTopm10.setDuration(50);

        ObjectAnimator translatePopFromTop = ObjectAnimator.ofFloat(view, "translationY", 0);
        translatePopFromTop.setDuration(50);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(translatePopFromTopp40);
        animatorSet.play(translatePopFromTopm30).after(translatePopFromTopp40);
        animatorSet.play(translatePopFromTopp20).after(translatePopFromTopm30);
        animatorSet.play(translatePopFromTopm10).after(translatePopFromTopp20);
        animatorSet.play(translatePopFromTop).after(translatePopFromTopm10);
        animatorSet.start();
    }

    public static void translate(View view, float fromPoint, float toPoint, Integer duration) {
        view.setVisibility(View.VISIBLE);
        view.setY(fromPoint);
        final ObjectAnimator translate = ObjectAnimator.ofFloat(view, "translationY", toPoint);
        translate.setDuration(duration);
        translate.start();
    }

    public static void translateX(View view, float fromPoint, float toPoint, Integer duration) {
        view.setVisibility(View.VISIBLE);
        view.setX(fromPoint);
        final ObjectAnimator translate = ObjectAnimator.ofFloat(view, "translationX", toPoint);
        translate.setDuration(duration);
        translate.start();
    }

    public static void translatePopToTop(final View view) {
        view.setVisibility(View.VISIBLE);

        ObjectAnimator translatePopFromTopp20 = ObjectAnimator.ofFloat(view, "translationY", 20);
        translatePopFromTopp20.setDuration(200);

        ObjectAnimator translatePopFromTopmh = ObjectAnimator.ofFloat(view, "translationY", -view.getHeight());
        translatePopFromTopmh.setDuration(200);

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(translatePopFromTopmh).after(translatePopFromTopp20);
        animatorSet.setInterpolator(new AccelerateInterpolator());
        animatorSet.start();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.setVisibility(View.GONE);
            }
        }, 400);
    }

    public static void popView(final View view, int duration) {
        view.setVisibility(View.VISIBLE);

        view.setScaleX(0.0f);
        view.setScaleY(0.0f);

        ObjectAnimator scaleUpX = ObjectAnimator.ofFloat(view, "scaleX", 1.0f);
        ObjectAnimator scaleUpY = ObjectAnimator.ofFloat(view, "scaleY", 1.0f);

        scaleUpX.setDuration(duration);
        scaleUpY.setDuration(duration);

        AnimatorSet scaleUp = new AnimatorSet();
        scaleUp.play(scaleUpX).with(scaleUpY);
        scaleUp.start();
    }
}
