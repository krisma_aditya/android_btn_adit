package com.danareksa.investasik.util;

import android.text.TextUtils;

import com.google.gson.Gson;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

/**
 * Created by reza.setyawan on 15/08/2017.
 */

public class StringUtility {

    public static String doMD5(String s) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(s.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }

    public static boolean isNotNull(String text){
        if (text == null){
            return false;
        }
        text = text.trim();
        return !text.equals("");
    }

    public static String priceFormatter(int amount) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("Rp ");
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
        nf.setMaximumFractionDigits(0);
        String amountOnString = nf.format(amount);
        amountOnString = amountOnString.replaceAll(",",".");
        return amountOnString;
    }

    public static String priceFormatter(double amount){
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("Rp ");
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
        nf.setMaximumFractionDigits(0);
        String amountOnString = nf.format(amount);
        amountOnString = amountOnString.replaceAll(",",".");
        return amountOnString;
    }

    public static String priceFormatter(float amount) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("Rp ");
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
        nf.setMaximumFractionDigits(0);
        String amountOnString = nf.format(amount);
        amountOnString = amountOnString.replaceAll(",",".");
        return amountOnString;
    }

    public static String priceFormatter(long amount) {
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("Rp ");
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
        nf.setMaximumFractionDigits(0);
        String amountOnString = nf.format(amount);
        amountOnString = amountOnString.replaceAll(",",".");
        return amountOnString;
    }

    public static String priceFormatter(BigDecimal amount) {
        if (amount == null) {
            return "0";
        }
        NumberFormat nf = NumberFormat.getCurrencyInstance();
        DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
        decimalFormatSymbols.setCurrencySymbol("Rp ");
        ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
        nf.setMaximumFractionDigits(0);
        String amountOnString = nf.format(amount);
        amountOnString = amountOnString.replaceAll(",",".");
        return amountOnString;
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }


    public static String printObjectJson(Object object){
        String result = "";
        if(object != null){
            Gson gson = new Gson();
            result = gson.toJson(object);
        }
        return result;
    }

}
