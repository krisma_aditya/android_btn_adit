package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Rizal Gunawan on 7/25/18.
 */

public class InvestmentAccountGroup implements Serializable{

    @SerializedName("LUMPSUM")
    @Expose
    private ArrayList<InvestmentAccountInfo> lumpsum = new ArrayList<>();

    @SerializedName("REGULER")
    @Expose
    private ArrayList<InvestmentAccountInfo> reguler = new ArrayList<>();

    public ArrayList<InvestmentAccountInfo> getLumpsum() {
        return lumpsum;
    }

    public void setLumpsum(ArrayList<InvestmentAccountInfo> lumpsum) {
        this.lumpsum = lumpsum;
    }

    public ArrayList<InvestmentAccountInfo> getReguler() {
        return reguler;
    }

    public void setReguler(ArrayList<InvestmentAccountInfo> reguler) {
        this.reguler = reguler;
    }
}
