package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.DocumentSettlement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pandu.abbiyuarsyah on 27/02/2017.
 */

public class CustomerDocumentSettlementResponse {

    @SerializedName("data")
    @Expose
    private DocumentSettlement data;

    /**
     * @return The data
     */
    public DocumentSettlement getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(DocumentSettlement data) {
        this.data = data;
    }

}
