package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 28/09/2018.
 */

public class ResponseBcaKlikpay implements Serializable{

    @SerializedName("redirectData")
    @Expose
    private RedirectDataBca data;

    @SerializedName("insertMessage")
    @Expose
    private String insertMessage;

    @SerializedName("insertStatus")
    @Expose
    private String insertStatus;

    @SerializedName("redirectURL")
    @Expose
    private String redirectURL;

    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("additionalData")
    @Expose
    private String additionalData;

    @SerializedName("channelId")
    @Expose
    private String channelId;

    @SerializedName("insertId")
    @Expose
    private String insertId;


    public RedirectDataBca getData() {
        return data;
    }

    public void setData(RedirectDataBca data) {
        this.data = data;
    }

    public String getInsertMessage() {
        return insertMessage;
    }

    public void setInsertMessage(String insertMessage) {
        this.insertMessage = insertMessage;
    }

    public String getInsertStatus() {
        return insertStatus;
    }

    public void setInsertStatus(String insertStatus) {
        this.insertStatus = insertStatus;
    }

    public String getRedirectURL() {
        return redirectURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAdditionalData() {
        return additionalData;
    }

    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getInsertId() {
        return insertId;
    }

    public void setInsertId(String insertId) {
        this.insertId = insertId;
    }
}
