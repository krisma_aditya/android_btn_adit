package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.PromoResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pandu.abbiyuarsyah on 18/05/2017.
 */

public class PromoListResponse extends com.danareksa.investasik.data.api.responses.GenericResponse implements Serializable{


    @SerializedName("data")
    @Expose
    private List<PromoResponse> data = new ArrayList<PromoResponse>();

    /**
     * @return The data
     */
    public List<PromoResponse> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<PromoResponse> data) {
        this.data = data;
    }

}
