package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.InvestmentList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by fajarfatur on 3/1/16.
 */
public class PortfolioInvestmentListResponse extends com.danareksa.investasik.data.api.responses.GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private InvestmentList data = new InvestmentList();

    public InvestmentList getData() {
        return data;
    }

    public void setData(InvestmentList data) {
        this.data = data;
    }
}
