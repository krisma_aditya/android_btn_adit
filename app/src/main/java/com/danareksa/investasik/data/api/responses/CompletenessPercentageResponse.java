package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Completeness;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by fajarfatur on 2/23/16.
 */
public class CompletenessPercentageResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private Completeness data;

    /**
     * @return The data
     */
    public Completeness getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(Completeness data) {
        this.data = data;
    }

}
