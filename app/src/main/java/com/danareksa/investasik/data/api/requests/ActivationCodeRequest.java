package com.danareksa.investasik.data.api.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 03/07/2018.
 */

public class ActivationCodeRequest {

    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("activationCode")
    @Expose
    private String activationCode;


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

}
