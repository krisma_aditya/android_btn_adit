package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.DocumentSignature;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pandu.abbiyuarsyah on 27/02/2017.
 */

public class CustomerDocumentSignatureResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {
    @SerializedName("data")
    @Expose
    private DocumentSignature data;

    /**
     * @return The data
     */
    public DocumentSignature getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(DocumentSignature data) {
        this.data = data;
    }

}
