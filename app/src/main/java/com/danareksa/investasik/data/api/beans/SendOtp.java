package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 23/08/2018.
 */

public class SendOtp {

    @SerializedName("token_otp")
    @Expose
    private String token_otp;

    public String getToken_otp() {
        return token_otp;
    }

    public void setToken_otp(String token_otp) {
        this.token_otp = token_otp;
    }
}
