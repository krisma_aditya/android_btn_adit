package com.danareksa.investasik.data.api.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 17/10/2018.
 */

public class BcaKlikpayConfirmationRequest {

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("transactionNo")
    @Expose
    private String transactionNo;


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }
}
