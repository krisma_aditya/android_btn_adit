package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 28/09/2018.
 */

public class RedirectDataBca implements Serializable{

    @SerializedName("klikPayCode")
    @Expose
    private String klikPayCode;

    @SerializedName("totalAmount")
    @Expose
    private String totalAmount;

    @SerializedName("payType")
    @Expose
    private String payType;

    @SerializedName("signature")
    @Expose
    private String signature;

    @SerializedName("transactionNo")
    @Expose
    private String transactionNo;

    @SerializedName("callback")
    @Expose
    private String callback;

    @SerializedName("currency")
    @Expose
    private String currency;

    @SerializedName("transactionDate")
    @Expose
    private String transactionDate;

    @SerializedName("miscFee")
    @Expose
    private String miscFee;

    @SerializedName("descp")
    @Expose
    private String descp;


    public String getKlikPayCode() {
        return klikPayCode;
    }

    public void setKlikPayCode(String klikPayCode) {
        this.klikPayCode = klikPayCode;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getMiscFee() {
        return miscFee;
    }

    public void setMiscFee(String miscFee) {
        this.miscFee = miscFee;
    }

    public String getDescp() {
        return descp;
    }

    public void setDescp(String descp) {
        this.descp = descp;
    }
}
