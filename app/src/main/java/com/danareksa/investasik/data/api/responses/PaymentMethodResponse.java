package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.PaymentList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pandu.abbiyuarsyah on 24/05/2017.
 */

public class PaymentMethodResponse extends com.danareksa.investasik.data.api.responses.GenericResponse implements Serializable{


    @SerializedName("data")
    @Expose
    private List<PaymentList> data = new ArrayList<PaymentList>();

    /**
     * @return The data
     */
    public List<PaymentList> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<PaymentList> data) {
        this.data = data;
    }

}
