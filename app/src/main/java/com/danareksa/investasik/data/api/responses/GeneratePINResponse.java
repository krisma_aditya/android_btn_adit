package com.danareksa.investasik.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ivan.pradana on 3/14/2017.
 */

public class GeneratePINResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("pin")
    @Expose
    private String pin;

    /*
    @SerializedName("token")
    @Expose
    private List<TokenPIN> listToken = new ArrayList<>();

    public List<TokenPIN> getListToken() {
        return listToken;
    }

    public void setListToken(List<TokenPIN> listToken) {
        this.listToken = listToken;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }
    */
}
