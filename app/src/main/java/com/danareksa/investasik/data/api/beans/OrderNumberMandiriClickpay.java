package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 07/09/2018.
 */

public class OrderNumberMandiriClickpay {


    @SerializedName("trxNo")
    @Expose
    private String trxNo;

    @SerializedName("adminFee")
    @Expose
    private Double adminFee;

    public String getTrxNo() {
        return trxNo;
    }

    public void setTrxNo(String trxNo) {
        this.trxNo = trxNo;
    }

    public Double getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(Double adminFee) {
        this.adminFee = adminFee;
    }
}
