package com.danareksa.investasik.data.api.beans;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by asep.surahman on 07/08/2018.
 */

public class InvestAccount implements Serializable {

    @SerializedName("cif")
    @Expose
    private String cif;
    @SerializedName("bankAccountNumber")
    @Expose
    private String bankAccountNumber;
    @SerializedName("bankName")
    @Expose
    private String bankName;
    @SerializedName("fullName")
    @Expose
    private String fullName;
    @SerializedName("bankAccountName")
    @Expose
    private String bankAccountName;
    @SerializedName("idNumber")
    @Expose
    private String idNumber;
    @SerializedName("heirList")
    @Expose
    private List<HeirList> heirList;
    @SerializedName("packageList")
    @Expose
    private List<PackageDetail> packageList;

    public String getCif() {
        return cif;
    }

    public void setCif(String cif) {
        this.cif = cif;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public List<HeirList> getHeirList() {
        return heirList;
    }

    public void setHeirList(List<HeirList> heirList) {
        this.heirList = heirList;
    }

    public List<PackageDetail> getPackageList() {
        return packageList;
    }

    public void setPackageList(List<PackageDetail> packageList) {
        this.packageList = packageList;
    }


}
