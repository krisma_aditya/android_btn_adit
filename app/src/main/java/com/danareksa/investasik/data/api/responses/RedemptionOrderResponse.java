package com.danareksa.investasik.data.api.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by pandu.abbiyuarsyah on 29/03/2017.
 */

public class RedemptionOrderResponse extends com.danareksa.investasik.data.api.responses.GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private RedemptionOrderDetailResponse data;

    /**
     * @return The data
     */
    public RedemptionOrderDetailResponse getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(RedemptionOrderDetailResponse data) {
        this.data = data;
    }
}
