package com.danareksa.investasik.data.api.requests;

import com.danareksa.investasik.data.api.beans.AddProductReguler;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 13/08/2018.
 */

public class AddProductRegulerRequest extends AddProductReguler implements Serializable {

    @SerializedName("token")
    @Expose
    private String token;
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
