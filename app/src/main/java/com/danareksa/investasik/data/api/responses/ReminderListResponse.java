package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Reminder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReminderListResponse extends GenericResponse {

    @SerializedName("data")
    @Expose
    private List<Reminder> data;

    public List<Reminder> getData() {
        return data;
    }

    public void setData(List<Reminder> data) {
        this.data = data;
    }
}
