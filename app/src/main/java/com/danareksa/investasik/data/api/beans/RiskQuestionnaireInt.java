package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 20/07/2018.
 */

public class RiskQuestionnaireInt {

    @SerializedName("riskQuestionId")
    @Expose
    private int riskQuestionId;

    @SerializedName("riskAnswerId")
    @Expose
    private int riskAnswerId;

    public int getRiskQuestionId() {
        return riskQuestionId;
    }

    public void setRiskQuestionId(int riskQuestionId) {
        this.riskQuestionId = riskQuestionId;
    }

    public int getRiskAnswerId() {
        return riskAnswerId;
    }

    public void setRiskAnswerId(int riskAnswerId) {
        this.riskAnswerId = riskAnswerId;
    }
}
