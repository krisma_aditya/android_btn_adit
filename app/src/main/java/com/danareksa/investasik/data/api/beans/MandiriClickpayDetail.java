package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 27/08/2018.
 */

public class MandiriClickpayDetail {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("debitCardNumber")
    @Expose
    private String debitCardNumber;


    @SerializedName("mandiriClickpayToken")
    @Expose
    private String mandiriClickpayToken;


    @SerializedName("netAmount")
    @Expose
    private Integer netAmount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDebitCardNumber() {
        return debitCardNumber;
    }

    public void setDebitCardNumber(String debitCardNumber) {
        this.debitCardNumber = debitCardNumber;
    }

    public String getMandiriClickpayToken() {
        return mandiriClickpayToken;
    }

    public void setMandiriClickpayToken(String mandiriClickpayToken) {
        this.mandiriClickpayToken = mandiriClickpayToken;
    }

    public Integer getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Integer netAmount) {
        this.netAmount = netAmount;
    }

    /*
    public Double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Double netAmount) {
        this.netAmount = netAmount;
    }*/
}
