package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asep.surahman on 08/10/2018.
 */

public class InvestmentListByProductType implements Serializable{


    @SerializedName("investmentComposition")
    @Expose
    private List<PortfolioProductComposition> investmentComposition = new ArrayList<>();

    @SerializedName("class")
    @Expose
    private String classData;

    @SerializedName("investmentAccountNumber")
    @Expose
    private String investmentAccountNumber;

    @SerializedName("investmentAmount")
    @Expose
    private Double investmentAmount;

    @SerializedName("investmentId")
    @Expose
    private String investmentId;

    @SerializedName("packageId")
    @Expose
    private Integer packageId;

    @SerializedName("packageImageKey")
    @Expose
    private String packageImageKey;

    @SerializedName("packageName")
    @Expose
    private String packageName;

    @SerializedName("taggedGoalName")
    @Expose
    private String taggedGoalName;

    @SerializedName("totalInvestmentMarketValue")
    @Expose
    private Double totalInvestmentMarketValue;

    public List<PortfolioProductComposition> getInvestmentComposition() {
        return investmentComposition;
    }

    public void setInvestmentComposition(List<PortfolioProductComposition> investmentComposition) {
        this.investmentComposition = investmentComposition;
    }

    public String getClassData() {
        return classData;
    }

    public void setClassData(String classData) {
        this.classData = classData;
    }

    public String getInvestmentAccountNumber() {
        return investmentAccountNumber;
    }

    public void setInvestmentAccountNumber(String investmentAccountNumber) {
        this.investmentAccountNumber = investmentAccountNumber;
    }

    public Double getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(Double investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    public String getInvestmentId() {
        return investmentId;
    }

    public void setInvestmentId(String investmentId) {
        this.investmentId = investmentId;
    }

    public Integer getPackageId() {
        return packageId;
    }

    public void setPackageId(Integer packageId) {
        this.packageId = packageId;
    }

    public String getPackageImageKey() {
        return packageImageKey;
    }

    public void setPackageImageKey(String packageImageKey) {
        this.packageImageKey = packageImageKey;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getTaggedGoalName() {
        return taggedGoalName;
    }

    public void setTaggedGoalName(String taggedGoalName) {
        this.taggedGoalName = taggedGoalName;
    }

    public Double getTotalInvestmentMarketValue() {
        return totalInvestmentMarketValue;
    }

    public void setTotalInvestmentMarketValue(Double totalInvestmentMarketValue) {
        this.totalInvestmentMarketValue = totalInvestmentMarketValue;
    }
}
