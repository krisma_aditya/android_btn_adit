package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.MaxScore;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by pandu.abbiyuarsyah on 03/07/2017.
 */

public class MaxScoreResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private MaxScore data;

    public MaxScore getData() {
        return data;
    }

    public void setData(MaxScore data) {
        this.data = data;
    }

}
