package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 23/08/2018.
 */

public class OtpResult {

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("trx_id")
    @Expose
    private String trx_id;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTrx_id() {
        return trx_id;
    }

    public void setTrx_id(String trx_id) {
        this.trx_id = trx_id;
    }
}
