package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.OrderNumberMandiriClickpay;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 07/09/2018.
 */

public class MandiriClickpayOrderNumberResponse extends GenericResponse {

    @SerializedName("data")
    @Expose
    private OrderNumberMandiriClickpay data;

    public OrderNumberMandiriClickpay getData() {
        return data;
    }

    public void setData(OrderNumberMandiriClickpay data) {
        this.data = data;
    }
}
