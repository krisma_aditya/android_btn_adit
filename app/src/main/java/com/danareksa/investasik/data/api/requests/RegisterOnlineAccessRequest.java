package com.danareksa.investasik.data.api.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 03/07/2018.
 */

public class RegisterOnlineAccessRequest {

    @SerializedName("ifua")
    @Expose
    private String ifua;
    @SerializedName("email")
    @Expose
    private String email;

    public String getIfua() {
        return ifua;
    }

    public void setIfua(String ifua) {
        this.ifua = ifua;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
