package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.Packages;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by glenrynaldi on 4/13/16.
 */
public class PackageByTokenResponse extends com.danareksa.investasik.data.api.responses.GenericResponse {

    @SerializedName("data")
    @Expose
    private List<Packages> data = new ArrayList<>();

    /**
     * @return The data
     */
    public List<Packages> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<Packages> data) {
        this.data = data;
    }

}
