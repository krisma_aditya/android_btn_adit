package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.PendingOrderDetail;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by pandu.abbiyuarsyah on 30/03/2017.
 */

public class PendingOrderDetailResponse extends com.danareksa.investasik.data.api.responses.GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private PendingOrderDetail data;

    /**
     * @return The data
     */
    public PendingOrderDetail getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(PendingOrderDetail data) {
        this.data = data;
    }

}
