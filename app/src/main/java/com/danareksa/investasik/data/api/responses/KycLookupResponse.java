package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.KycLookup;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by asep.surahman on 05/07/2018.
 */

public class KycLookupResponse extends GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private List<KycLookup> kycLookupList;


    public List<KycLookup> getKycLookupList() {
        return kycLookupList;
    }

    public void setKycLookupList(List<KycLookup> kycLookupList) {
        this.kycLookupList = kycLookupList;
    }
}
