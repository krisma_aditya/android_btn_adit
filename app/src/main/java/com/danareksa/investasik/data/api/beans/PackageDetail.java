package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by asep.surahman on 08/08/2018.
 */

public class PackageDetail implements Serializable{


    @SerializedName("monthlyInvestmentAmount")
    @Expose
    private Double monthlyInvestmentAmount;

    @SerializedName("totalMonthlyAutodebet")
    @Expose
    private Double totalMonthlyAutodebet;

    @SerializedName("monthlyInvestmentFee")
    @Expose
    private Double monthlyInvestmentFee;


    @SerializedName("regulerPeriod")
    @Expose
    private String regulerPeriod;

    @SerializedName("packageName")
    @Expose
    private String packageName;


    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }


    public String getRegulerPeriod() {
        return regulerPeriod;
    }

    public void setRegulerPeriod(String regulerPeriod) {
        this.regulerPeriod = regulerPeriod;
    }

    public Double getMonthlyInvestmentAmount() {
        return monthlyInvestmentAmount;
    }

    public void setMonthlyInvestmentAmount(Double monthlyInvestmentAmount) {
        this.monthlyInvestmentAmount = monthlyInvestmentAmount;
    }

    public Double getTotalMonthlyAutodebet() {
        return totalMonthlyAutodebet;
    }

    public void setTotalMonthlyAutodebet(Double totalMonthlyAutodebet) {
        this.totalMonthlyAutodebet = totalMonthlyAutodebet;
    }

    public Double getMonthlyInvestmentFee() {
        return monthlyInvestmentFee;
    }

    public void setMonthlyInvestmentFee(Double monthlyInvestmentFee) {
        this.monthlyInvestmentFee = monthlyInvestmentFee;
    }
}
