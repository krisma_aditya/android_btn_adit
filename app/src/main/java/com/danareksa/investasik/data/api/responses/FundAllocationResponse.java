package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.FundAllocation;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by fajarfatur on 2/23/16.
 */
public class FundAllocationResponse extends com.danareksa.investasik.data.api.responses.GenericResponse implements Serializable {

    @SerializedName("data")
    @Expose
    private List<FundAllocation> data = new ArrayList<>();


    /**
     *
     * @return
     * The data
     */
    public List<FundAllocation> getData() {
        return data;
    }

    /**
     *
     * @param data
     * The data
     */
    public void setData(List<FundAllocation> data) {
        this.data = data;
    }

}
