package com.danareksa.investasik.data.api.responses;
import com.danareksa.investasik.data.api.beans.OtpResult;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 23/08/2018.
 */

public class OtpRequestResponse extends GenericResponse {

    @SerializedName("data")
    @Expose
    private OtpResult data;

    public OtpResult getData() {
        return data;
    }

    public void setData(OtpResult data) {
        this.data = data;
    }
}
