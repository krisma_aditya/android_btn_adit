package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by asep.surahman on 30/04/2018.
 */

public class Article {

    @SerializedName("newsContent")
    @Expose
    private String newsContent;

    @SerializedName("newsTitle")
    @Expose
    private String newsTitle;

    @SerializedName("publishedDate")
    @Expose
    private String publishedDate;

    @SerializedName("imageLocation")
    @Expose
    private String imageLocation;


    public String getNewsContent() {
        return newsContent;
    }

    public void setNewsContent(String newsContent) {
        this.newsContent = newsContent;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }

    public String getImageLocation() {
        return imageLocation;
    }

    public void setImageLocation(String imageLocation) {
        this.imageLocation = imageLocation;
    }
}
