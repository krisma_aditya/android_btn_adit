package com.danareksa.investasik.data.api.responses;

import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by asep.surahman on 08/10/2018.
 */

public class InvestmentListByProductTypeResponse extends GenericResponse implements Serializable {




    @SerializedName("data")
    @Expose
    private List<PortfolioInvestment> data = new ArrayList<>();

    public List<PortfolioInvestment> getData() {
        return data;
    }

    public void setData(List<PortfolioInvestment> data) {
        this.data = data;
    }

    /* @SerializedName("data")
    @Expose
    private List<InvestmentListByProductType> data = new ArrayList<>();


    public List<InvestmentListByProductType> getData() {
        return data;
    }

    public void setData(List<InvestmentListByProductType> data) {
        this.data = data;
    }*/
}
