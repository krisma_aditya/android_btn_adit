package com.danareksa.investasik.data.api.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by fajarfatur on 3/6/16.
 */
public class TransactionHistory implements Serializable {

    @SerializedName("pkg_image")
    @Expose
    private String pkg_image;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("order_number")
    @Expose
    private String orderNumber;
    @SerializedName("transaction_date")
    @Expose
    private String transactionDate;
    @SerializedName("transaction_time")
    @Expose
    private String transactionTime;
    @SerializedName("investment_number")
    @Expose
    private String investmentNumber;
    @SerializedName("package_name")
    @Expose
    private String packageName;
    @SerializedName("amount")
    @Expose
    private double amount;
    @SerializedName("payment_type")
    @Expose
    private String paymentType;
    @SerializedName("transaction_type")
    @Expose
    private String transactionType;
    @SerializedName("transaction_status")
    @Expose
    private String transactionStatus;
    @SerializedName("uploaded")
    @Expose
    private Integer uploaded;

    @SerializedName("unit")
    @Expose
    private double unit;

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getInvestmentNumber() {
        return investmentNumber;
    }

    public void setInvestmentNumber(String investmentNumber) {
        this.investmentNumber = investmentNumber;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public Integer getUploaded() {
        return uploaded;
    }

    public void setUploaded(Integer uploaded) {
        this.uploaded = uploaded;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getUnit() {
        return unit;
    }

    public void setUnit(double unit) {
        this.unit = unit;
    }

    public String getPkg_image() {
        return pkg_image;
    }

    public void setPkg_image(String pkg_image) {
        this.pkg_image = pkg_image;
    }
}
