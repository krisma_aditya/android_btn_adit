package com.danareksa.investasik.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.ProductList;
import com.danareksa.investasik.ui.fragments.product.ChooseIfuaFragment;
import com.google.gson.Gson;

import butterknife.Bind;
import icepick.State;

/**
 * Created by asep.surahman on 11/06/2018.
 */

public class ChooseIfuaActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;
    @State
    ProductList product;

    @State
    Packages packages;

    public static Activity choIfuaActivity;

    private final static String PRODUCT = "product";
    private final static String PACKAGES = "packages";


    public static void startActivity(BaseActivity sourceActivity, ProductList product, Packages packages){
        Intent intent = new Intent(sourceActivity, ChooseIfuaActivity.class);
        intent.putExtra(PRODUCT, product);
        intent.putExtra(PACKAGES, packages);
        sourceActivity.startActivity(intent);
    }

    public static void startActivity(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, ChooseIfuaActivity.class);
        sourceActivity.startActivity(intent);
    }


    @Override
    protected int getLayout() {
        return R.layout.a_choose_ifua;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        choIfuaActivity = this;
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(null);
        }
        product  = (ProductList) getIntent().getSerializableExtra(PRODUCT);
        packages = (Packages) getIntent().getSerializableExtra(PACKAGES);

        Gson gson = new Gson();
        String prod = gson.toJson(product);
        title.setText("Pilih Kode Investor (IFUA)");
        ChooseIfuaFragment.showFragment(this, product, packages);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        //ListOfCatalogueActivity.startActivity(this);
        this.finish();
    }


}
