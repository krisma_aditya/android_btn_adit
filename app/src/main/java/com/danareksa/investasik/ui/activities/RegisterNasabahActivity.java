package com.danareksa.investasik.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.fragments.registernasabah.RegisterNasabahMainFragment;
import com.danareksa.investasik.ui.fragments.registernasabah.RegisterNasabahlamaFragment;

import butterknife.Bind;

/**
 * Created by asep.surahman on 08/05/2018.
 */

public class RegisterNasabahActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    public static Activity regNasabahActivity;

    public static void startActivity(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, RegisterNasabahActivity.class);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_register_data_pribadi;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        regNasabahActivity = this;
        setupToolbar();
        title.setTypeface(Typeface.createFromAsset(getAssets(),"fonts/HelveticaNeueLts.otf"));

        if(BuildConfig.FLAVOR.contentEquals("btn")){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
            RegisterNasabahlamaFragment.showFragment(this);
        } else if (BuildConfig.FLAVOR.contentEquals("dim")){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
            RegisterNasabahMainFragment.showFragment(this);
        } else {
            RegisterNasabahMainFragment.showFragment(this);
        }
    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        title.setText("Registrasi");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        android.support.v4.app.Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.container);
        if(currentFragment instanceof RegisterNasabahMainFragment) {

           boolean visible = ((RegisterNasabahMainFragment) currentFragment).isPageVisible();

           if (!visible) {
               super.onBackPressed();
               startActivity(new Intent(RegisterNasabahActivity.this, LandingPageActivity.class));
               finish();
           } else {
               ((RegisterNasabahMainFragment) currentFragment).handleBackPressed();
           }

        } else {
            super.onBackPressed();
            startActivity(new Intent(RegisterNasabahActivity.this, LandingPageActivity.class));
            finish();
        }
    }


    public void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }



}
