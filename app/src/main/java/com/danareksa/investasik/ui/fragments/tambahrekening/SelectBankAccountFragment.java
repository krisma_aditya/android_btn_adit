package com.danareksa.investasik.ui.fragments.tambahrekening;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.BankAccount;
import com.danareksa.investasik.data.api.beans.PackageListProduct;
import com.danareksa.investasik.data.api.requests.AddAccountRequest;
import com.danareksa.investasik.util.eventBus.RxBusObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;




/**
 * Created by asep.surahman on 25/07/2018.
 */

public class SelectBankAccountFragment extends BaseInvest{

    public static final String TAG = SelectBankAccountFragment.class.getSimpleName();
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.bNext)
    Button bNext;
    @Bind(R.id.sRekening)
    Spinner sRekening;

    List<BankAccount> bankAccountList;
    SelectBankAccountPresenter presenter;
    private String jsonCountry;
    private String jsonBank;
    private String type;
    public PackageListProduct packageListProduct;
    public static final String ADD_ACCOUNT_REQUEST = "accountRequest";
    int bankAccountId = 0;


    public static Fragment getFragment(String jsonCountry, String jsonBank, String typeAccount, AddAccountRequest acountRequest) {
        Bundle bundle = new Bundle();
        bundle.putString("jsonCountry", jsonCountry);
        bundle.putString("jsonBank", jsonBank);
        bundle.putString("type", typeAccount);
        bundle.putSerializable(ADD_ACCOUNT_REQUEST, acountRequest);
        SelectBankAccountFragment selectBankAccountFragment = new SelectBankAccountFragment();
        selectBankAccountFragment.setArguments(bundle);
        return selectBankAccountFragment;
    }


    @Override
    protected int getLayout() {
        return R.layout.f_tambah_rekening_pilih_bank;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bankAccountList = new ArrayList<>();
        jsonCountry = getArguments().getString("jsonCountry");
        jsonBank = getArguments().getString("jsonBank");
        type = getArguments().getString("type");
        packageListProduct = (PackageListProduct) getArguments().getSerializable("packageList");
        presenter = new SelectBankAccountPresenter(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onItemSelectedItem();
        presenter.getCustomerData(type);
    }



    public void onItemSelectedItem(){
        sRekening.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l){
                if(bankAccountList != null && bankAccountList.size() != 0){
                    bankAccountId = Integer.valueOf(bankAccountList.get(sRekening.getSelectedItemPosition()).getBankAccountId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView){
            }
        });

    }




    @Override
    public void onResume(){
        super.onResume();
    }


    @OnClick(R.id.bNext)
    void bNext(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            setDataBankAccount();
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.TO_PAGE_HEIR, null));
        }
    }


    private void setDataBankAccount(){
        System.out.println("bank Account Id : " + bankAccountId);
        acountRequest.setBankAccountId(bankAccountId);
    }


    @OnClick(R.id.bChangeBank)
    void bChangeBank(){
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
    }


    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar() {
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError(){
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }


    public void fetchResultToLayout(){
        setupSpinnerBankAccount(sRekening, bankAccountList, "");
    }


    @Override
    public void nextWithoutValidation(){

    }

    @Override
    public void saveDataKycWithBackpress(){

    }

    @Override
    public void addAccountBackpress() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }


    public boolean validate(){
        boolean valid = true;
        if(sRekening.getSelectedItem().toString().equals("")){
            valid = false;
            ((TextView) sRekening.getSelectedView()).setError("Mohon isi kolom ini");
        }
        return  valid;
    }


    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }



}
