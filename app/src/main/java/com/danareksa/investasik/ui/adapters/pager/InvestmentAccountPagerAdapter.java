package com.danareksa.investasik.ui.adapters.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.danareksa.investasik.data.api.requests.AddAccountRequest;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.ui.fragments.tambahrekening.AccountDataFragment;
import com.danareksa.investasik.ui.fragments.tambahrekening.CustomerDataFragment;
import com.danareksa.investasik.ui.fragments.tambahrekening.HeirDataFragment;
import com.danareksa.investasik.ui.fragments.tambahrekening.SelectBankAccountFragment;
import com.danareksa.investasik.ui.fragments.tambahrekening.SelectReksaDanaFragment;

/**
 * Created by asep.surahman on 06/08/2018.
 */

public class InvestmentAccountPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 5;
    private BaseFragment bFragment;
    private String jsonCountry;
    private String jsonBank;
    private String typeAccount;
    private AddAccountRequest addAccountRequest;


    private String[] title = new String[] {"Isi data nasabah", "Pilih Reksa Dana",
            "Pilih rekening", "Isi data rekening", "Isi data ahli waris"
    };


    public InvestmentAccountPagerAdapter(BaseFragment bFragment, FragmentManager fm, String typeAccount, String jsonCountry, String jsonBank, AddAccountRequest addAccountRequest) {
        super(fm);
        this.bFragment = bFragment;
        this.jsonCountry = jsonCountry;
        this.jsonBank = jsonBank;
        this.typeAccount = typeAccount;
        this.addAccountRequest = addAccountRequest;
    }

    @Override
    public int getCount(){
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position){
        Fragment fragment = null;
        switch (position){
            case 0:
                fragment = CustomerDataFragment.getFragment(jsonCountry, jsonBank, typeAccount, addAccountRequest);
                break;
            case 1:
                fragment = SelectReksaDanaFragment.getFragment(jsonCountry, jsonBank, typeAccount, addAccountRequest);
                break;
            case 2:
                fragment = SelectBankAccountFragment.getFragment(jsonCountry, jsonBank, typeAccount, addAccountRequest);
                break;
            case 3:
                fragment = AccountDataFragment.getFragment(jsonCountry, jsonBank, typeAccount, addAccountRequest);
                break;
            case 4:
                fragment = HeirDataFragment.getFragment(typeAccount, addAccountRequest);
                break;
        }
        return fragment;
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return title[position];
    }


}
