package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;

import com.danareksa.investasik.R;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

/**
 * Created by pandu.abbiyuarsyah on 17/05/2017.
 */

public class YouTubePlayerActivity extends YouTubeBaseActivity {
    public static final String API_KEY = "AIzaSyDWu9Ow2jmmZ69mLTdMBg2tAuKJ-lJ0h38";

    //https://www.youtube.com/watch?v=<VIDEO_ID>
    public static String VIDEO_ID = "Bvts2MjdaxQ";
    public static  Boolean IS_FROM_LANDING = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.f_youtube_player);

        YouTubePlayerView youTubePlayerView =
                (YouTubePlayerView) findViewById(R.id.youtube_player_view);


        youTubePlayerView.initialize(API_KEY,
                new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                                        YouTubePlayer youTubePlayer, boolean b) {

                        youTubePlayer.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION |
                                YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE);

                        // do any work here to cue video, play video, etc.
                        youTubePlayer.loadVideo(VIDEO_ID);
                    }
                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider,
                                                        YouTubeInitializationResult youTubeInitializationResult) {

                    }
                });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        if (IS_FROM_LANDING) {
            startActivity(new Intent(YouTubePlayerActivity.this, LandingPageActivity.class));
            IS_FROM_LANDING = false;
        } else {
            startActivity(new Intent(YouTubePlayerActivity.this, MainActivity.class));
        }
        finish();
    }



}
