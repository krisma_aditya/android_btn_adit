package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.MainActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;

import butterknife.OnClick;

/**
 * Created by asep.surahman on 11/06/2018.
 */

public class Kyc15AdvancedFinishFragment extends BaseFragment {

    public static final String TAG = Kyc15AdvancedFinishFragment.class.getSimpleName();

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc15AdvancedFinishFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayout() {
        return R.layout.f_register_lanjut_selamat_finish;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    public void onResume(){
        super.onResume();
    }


    @OnClick(R.id.bFinish)
    void bFinish(){
        getActivity().finish();
        MainActivity.startActivity((BaseActivity) getActivity());
    }






}
