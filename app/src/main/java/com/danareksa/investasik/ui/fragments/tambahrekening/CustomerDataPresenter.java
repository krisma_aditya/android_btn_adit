package com.danareksa.investasik.ui.fragments.tambahrekening;

import com.danareksa.investasik.data.api.requests.CustomerDataRequest;
import com.danareksa.investasik.data.api.responses.CustomerDataRekeningResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 02/08/2018.
 */

public class CustomerDataPresenter{

    CustomerDataFragment fragment;

    public CustomerDataPresenter(CustomerDataFragment fragment){
        this.fragment = fragment;
    }

    public CustomerDataRequest getCustomerDataRequest(String accountType){
        CustomerDataRequest customerDataRequest = new CustomerDataRequest();
        customerDataRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        customerDataRequest.setAccountType(accountType);
        return customerDataRequest;

    }

    public void getCustomerData(final String accountType){
        fragment.showProgressBar();
        fragment.getApi().getCustomerData(getCustomerDataRequest(accountType))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<CustomerDataRekeningResponse>(){
                    @Override
                    public void onCompleted(){
                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(CustomerDataRekeningResponse customerDataRekeningResponse){
                        fragment.dismissProgressBar();
                        if(customerDataRekeningResponse.getCode() == 0){
                            fragment.fetchResultToLayout(customerDataRekeningResponse);
                        }else {
                            fragment.fetchResultFailed();
                            fragment.showDialog(customerDataRekeningResponse.getInfo());
                        }
                    }
                });
    }




}
