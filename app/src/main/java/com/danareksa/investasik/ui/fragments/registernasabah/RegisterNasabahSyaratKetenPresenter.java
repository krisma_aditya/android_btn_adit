package com.danareksa.investasik.ui.fragments.registernasabah;

import android.widget.Toast;

import com.danareksa.investasik.data.api.beans.RegisterNewCustomer;
import com.danareksa.investasik.data.api.beans.RegisterOnlineAccess;
import com.danareksa.investasik.data.api.requests.RegisterNasabahRequest;
import com.danareksa.investasik.data.api.requests.RegisterNewCustomerRequest;
import com.danareksa.investasik.data.api.requests.RegisterOnlineAccessRequest;
import com.danareksa.investasik.data.api.responses.GenericResponse;
import com.danareksa.investasik.data.api.responses.NewRegistrationResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.util.Crypto;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by asep.surahman on 29/06/2018.
 */

public class RegisterNasabahSyaratKetenPresenter {

    private RegisterNasabahSyaratKetenFragment fragment;
    public RegisterNasabahSyaratKetenPresenter(RegisterNasabahSyaratKetenFragment fragment){
        this.fragment = fragment;
    }

    public RegisterNewCustomerRequest constructRegisterNewCustomerRequest(RegisterNewCustomer registerNewCustomer){
        RegisterNewCustomerRequest request = new RegisterNewCustomerRequest();
        String email = getAndTrimValue(registerNewCustomer.getEmail());
        request.setEmail(email.toLowerCase());
        request.setFirstName(getAndTrimValue(registerNewCustomer.getFirstName()));
        request.setMiddleName(getAndTrimValue(registerNewCustomer.getMiddleName()));
        request.setLastName(getAndTrimValue(registerNewCustomer.getLastName()));
        request.setMobileNumber(getAndTrimValue(registerNewCustomer.getMobileNumber()));
        request.setBirthDate(getAndTrimValue(registerNewCustomer.getBirthDate()));
        request.setPassword(Crypto.Encrypt(getAndTrimValue(registerNewCustomer.getPassword())));
        return request;
    }


    public RegisterOnlineAccessRequest constructRegisterOnlineAccessRequest(RegisterOnlineAccess registerOnlineAccessRequest) {
        RegisterOnlineAccessRequest request = new RegisterOnlineAccessRequest();
        String email = getAndTrimValue(registerOnlineAccessRequest.getEmail().toString());
        request.setEmail(email.toLowerCase());
        request.setIfua(getAndTrimValue(registerOnlineAccessRequest.getIfua().toString()));
        return request;
    }



    void register(final RegisterNewCustomerRequest registerNewCustomer, final String email) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().regsiterNewCustomer(
                registerNewCustomer)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<NewRegistrationResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressDialog();
                        Timber.e(e.getLocalizedMessage());
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }


                    @Override
                    public void onNext(NewRegistrationResponse genericResponse) {
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), genericResponse.getInfo(), Toast.LENGTH_SHORT).show();
                        if (genericResponse.getCode() == fragment.successCode){

                            fragment.fullName = getFullName(registerNewCustomer);
                            PrefHelper.setString(PrefKey.FIRST_NAME, fragment.fullName);
                            fragment.gotoActivationCodeActivity(genericResponse.getData().getUsername(), registerNewCustomer.getPassword(), email);
                        }else{
                            fragment.showDialog(genericResponse.getInfo());
                        }
                    }
                });
    }




    void registerOnlineAccess(final RegisterOnlineAccessRequest registerOnlineAccessRequest) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().regsiterOnlineAccess(registerOnlineAccessRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressDialog();
                        Timber.e(e.getLocalizedMessage());
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(GenericResponse genericResponse) {
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), genericResponse.getInfo(), Toast.LENGTH_SHORT).show();
                        if (genericResponse.getCode() == fragment.successCode) {
                            fragment.showDialogKonfirmasi();
                        }else{
                            fragment.showDialog(genericResponse.getInfo());
                        }
                    }
                });
    }

    public RegisterNasabahRequest constructRegisterNasabahRequest(RegisterNasabahRequest registerNasabahRequest) {
        RegisterNasabahRequest request = new RegisterNasabahRequest();
        String email = getAndTrimValue(registerNasabahRequest.getEmail().toString());

        request.setNik(registerNasabahRequest.getNik());
        request.setEmail(email.toLowerCase());
        request.setMobilePhoneNo(registerNasabahRequest.getMobilePhoneNo().toString());
        return request;
    }

    void registerNasabah(final RegisterNasabahRequest registerNasabahRequest) {
        fragment.showProgressDialog(fragment.loading);
        fragment.getApi().regsiterBtnCustomer(registerNasabahRequest)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<GenericResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressDialog();
                        Timber.e(e.getLocalizedMessage());
                        Toast.makeText(fragment.getContext(), fragment.connectionError, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(GenericResponse genericResponse) {
                        fragment.dismissProgressDialog();
                        Toast.makeText(fragment.getContext(), genericResponse.getInfo(), Toast.LENGTH_SHORT).show();
                        if (genericResponse.getCode() == fragment.successCode) {
                            fragment.showDialogKonfirmasi();
                        }else{
                            fragment.showDialog(genericResponse.getInfo());
                        }
                    }
                });
    }


    private String getAndTrimValue(String e) {
        return e.toString().trim();
    }


    private String getFullName(RegisterNewCustomerRequest registerNewCustomer){
        String name = "";
        if(registerNewCustomer.getMiddleName().equals("") && registerNewCustomer.getLastName().equals("")){
            name = registerNewCustomer.getFirstName();
        }else if(!registerNewCustomer.getMiddleName().equals("") && registerNewCustomer.getLastName().equals("")){
            name = registerNewCustomer.getFirstName() + " " + registerNewCustomer.getMiddleName();
        }else if(registerNewCustomer.getMiddleName().equals("") && !registerNewCustomer.getLastName().equals("")){
            name = registerNewCustomer.getFirstName() + " " + registerNewCustomer.getLastName();
        }else if(!registerNewCustomer.getMiddleName().equals("") && !registerNewCustomer.getLastName().equals("")){
            name = registerNewCustomer.getFirstName() + " " + registerNewCustomer.getMiddleName()+ " " + registerNewCustomer.getLastName();
        }
        name = name.replace("  ", "");
        return name;
    }


}
