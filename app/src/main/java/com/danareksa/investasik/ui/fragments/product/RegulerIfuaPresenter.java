package com.danareksa.investasik.ui.fragments.product;

import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.requests.PackageListRequest;
import com.danareksa.investasik.data.api.requests.ValidateSubscRegulerRequest;
import com.danareksa.investasik.data.api.responses.PackageListRegulerResponse;
import com.danareksa.investasik.data.api.responses.ValidateIfuaRegulerResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 24/09/2018.
 */

public class RegulerIfuaPresenter {

    private RegulerIfuaFragment fragment;

    public RegulerIfuaPresenter(RegulerIfuaFragment fragment) {
        this.fragment = fragment;
    }

    public PackageListRequest getPackageListRequest(){
        PackageListRequest packageListRequest = new PackageListRequest();
        packageListRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        return packageListRequest;
    }


    public void getPackageList(){
        fragment.showProgressBar();
        fragment.getApi().getPackageListReguler(getPackageListRequest())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PackageListRegulerResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(PackageListRegulerResponse packageListRegulerResponse){
                        fragment.dismissProgressBar();
                        if(packageListRegulerResponse.getCode() == 0){
                            fragment.packageListRegulersAll = packageListRegulerResponse.getPackageListRegulers();
                        }
                    }
                });
    }


    public boolean checkIdPackage(Packages packages){
        boolean status = false;
        if(fragment.packageListRegulersAll != null && fragment.packageListRegulersAll.size() > 0){
            for(int i = 0; i < fragment.packageListRegulersAll.size(); i++){
                if(packages.getId() == fragment.packageListRegulersAll.get(i).getId()){
                    status = true;
                }
            }
        }
        return status;
    }




    public void getValidateIfuaReg(final String ifuaNumber, String productCode){
        fragment.showProgressBar();
        fragment.getApi().getValidateIfuaReguler(getValidateIfuaReguler(ifuaNumber, productCode))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<ValidateIfuaRegulerResponse>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(ValidateIfuaRegulerResponse packageListRegulerResponse){
                        fragment.dismissProgressBar();
                        if(packageListRegulerResponse.getCode() == 0 || packageListRegulerResponse.getCode() == 1){
                            fragment.fetchResultValidate(ifuaNumber);
                        }else{
                            fragment.showDialog(packageListRegulerResponse.getInfo());
                        }
                    }
                });
    }


    public ValidateSubscRegulerRequest getValidateIfuaReguler(String ifuaNumber, String productCode){
        ValidateSubscRegulerRequest validateSubscRegulerRequest = new ValidateSubscRegulerRequest();
        validateSubscRegulerRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        validateSubscRegulerRequest.setIfuaNumber(ifuaNumber);
        validateSubscRegulerRequest.setProductCode(productCode);
        return validateSubscRegulerRequest;
    }





}
