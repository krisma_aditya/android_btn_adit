package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.KycLookup;
import com.danareksa.investasik.data.api.beans.ListPackage;
import com.danareksa.investasik.data.api.beans.PackageListReguler;
import com.danareksa.investasik.data.api.beans.SubscriptionFee;
import com.danareksa.investasik.ui.fragments.tambahrekening.SelectReksaDanaPresenter;
import com.danareksa.investasik.util.AmountFormatter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by asep.surahman on 02/08/2018.
 */

public class SelectReksaDanaListAdapter extends RecyclerView.Adapter<SelectReksaDanaListAdapter.SelectReksaDanaHolder>{

    List<PackageListReguler> packageListRegulerAll;
    List<PackageListReguler> packageListReguler;
    private PackageListReguler itemPackageAll;
    private PackageListReguler itemPackage;
    List<ListPackage> listPackages;
    private List<SelectReksaDanaListAdapter.SelectReksaDanaHolder> holderList;
    List<KycLookup> kycLookupTimePeriod;

    private SelectReksaDanaPresenter presenter;
    private Context context;
    private CallbackPackage mCallback;

    public SelectReksaDanaListAdapter(SelectReksaDanaPresenter presenter, List<PackageListReguler> packageListReguler, List<PackageListReguler> packageListRegulerAll, Context context, CallbackPackage mCallbacks, List<KycLookup> kycLookupTimePeriod) {
        this.packageListReguler = packageListReguler;
        this.packageListRegulerAll = packageListRegulerAll;
        this.holderList = new ArrayList<>();
        this.context = context;
        this.presenter = presenter;
        this.kycLookupTimePeriod = kycLookupTimePeriod;
        mCallback = mCallbacks;
    }


    @Override
    public SelectReksaDanaListAdapter.SelectReksaDanaHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_select_reksa_dana, parent, false);
        return new SelectReksaDanaListAdapter.SelectReksaDanaHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final SelectReksaDanaListAdapter.SelectReksaDanaHolder holder, final int position){
        if (packageListRegulerAll == null) return;
        itemPackageAll = packageListRegulerAll.get(position);
        itemPackage = packageListReguler.get(position);
        holder.itemView.setTag(itemPackageAll);

        Type listType = new TypeToken<List<ListPackage>>(){}.getType();
        Gson gson = new Gson();
        String stringPackage = gson.toJson(getListPackage(packageListRegulerAll)).toString();
        listPackages = gson.fromJson(stringPackage, listType);

        setupSpinnerPackage(holder.sSelectReksaDana, listPackages, String.valueOf(itemPackage.getId()));
        setupSpinnerWithSpecificLookupSelection(holder.sTimePeriod, kycLookupTimePeriod, String.valueOf(itemPackage.getTimePeriod()));

        if(itemPackage.getId() != 0){
            holder.packageId = listPackages.get(holder.sSelectReksaDana.getSelectedItemPosition()).getId();
            holder.sTimePeriod.setClickable(true);
            holder.cbAccept.setEnabled(true);
            holder.edtAmount.setEnabled(true);
        }else{
            holder.packageId = 0;
            holder.sTimePeriod.setClickable(false);
            holder.cbAccept.setEnabled(false);
            holder.edtAmount.setEnabled(false);
        }

        if(!itemPackage.getTimePeriod().equals("")){
            holder.timePeriod = String.valueOf(kycLookupTimePeriod.get(holder.sTimePeriod.getSelectedItemPosition()).getValue());
        }else{
            holder.timePeriod = "";
        }

        holder.cbAccept.setChecked(itemPackage.isAccept());

        if(holder.cbAccept.isChecked()){
            holder.accept = true;
        }else{
            holder.accept = false;
        }

        if(itemPackage.getMinSubscription() != 0.0){
            holder.minSubcription = itemPackage.getMinSubscription();
        }else{
            holder.minSubcription = 0.0;
        }

        holder.sSelectReksaDana.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l){
                holder.packageId  = listPackages.get(holder.sSelectReksaDana.getSelectedItemPosition()).getId();
                String strPackage = "";
                strPackage = adapterView.getSelectedItem().toString();

                if(!strPackage.equals("")){
                    holder.cbAccept.setEnabled(true);
                    holder.sTimePeriod.setClickable(true);
                    holder.edtAmount.setEnabled(true);
                }else{
                    holder.cbAccept.setEnabled(false);
                    holder.sTimePeriod.setClickable(false);
                    holder.edtAmount.setEnabled(false);
                    holder.cbAccept.setChecked(false);
                    holder.cbAccept.setEnabled(false);

                    setupSpinnerWithSpecificLookupSelection(holder.sTimePeriod, kycLookupTimePeriod, "");
                    holder.edtAmount.setText("");
                    holder.edtFee.setText("");
                    holder.edtTotal.setText("");
                }

                if(!holder.edtAmount.getText().toString().equals("")){
                    for(PackageListReguler packageListReguler : packageListRegulerAll){
                        if(holder.packageId == packageListReguler.getId()){
                            itemPackage = packageListReguler;
                            for(int j = 0; j < packageListReguler.getSubscriptionFeeList().size(); j++){

                                Long amount = Long.parseLong(holder.edtAmount.getText().toString());
                                SubscriptionFee fee = packageListReguler.getSubscriptionFeeList().get(j);
                                Long amountMin = fee.getAmountMin().longValue();
                                Long amountMax = fee.getAmountMax().longValue();
                                if (amountMax == 0)
                                    amountMax = amount + 1;

                                if (amount >= amountMin && amount <= amountMax){

                                    double feeAmount = amount * fee.getFeePercentage();
                                    //long fAmount = (long) feeAmount;
                                    holder.edtFee.setText(AmountFormatter.formatFeePercent(fee.getFeePercentage()) + " | " + AmountFormatter.formatNonCurrency(feeAmount));
                                    Double total = amount + (amount * fee.getFeePercentage());
                                    holder.edtTotal.setText(AmountFormatter.formatCurrencyWithoutComma(total));
                                    holder.total = total;
                                }
                            }

                            //min subscription
                            holder.minSubcription = packageListReguler.getMinSubscription();

                        }
                    }
                }else{
                    for(PackageListReguler packageListReguler : packageListRegulerAll){
                        if (holder.packageId == packageListReguler.getId()) {
                            //min subscription
                            holder.minSubcription = packageListReguler.getMinSubscription();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView){

            }

        });


        holder.sTimePeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                holder.timePeriod = kycLookupTimePeriod.get(holder.sTimePeriod.getSelectedItemPosition()).getValue();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent){

            }
        });




        //===================if amount not == 0
        if(!itemPackage.getTransactionAmount().equals("")){
            holder.edtAmount.setText(itemPackage.getTransactionAmount());
            for(PackageListReguler packageListReguler : packageListRegulerAll){
                if(itemPackage.getId() == packageListReguler.getId()){
                    for(int i = 0; i < packageListReguler.getSubscriptionFeeList().size(); i++){

                        Long amount = Long.parseLong(itemPackage.getTransactionAmount().toString());
                        SubscriptionFee fee = packageListReguler.getSubscriptionFeeList().get(i);
                        Long amountMin = fee.getAmountMin().longValue();
                        Long amountMax = fee.getAmountMax().longValue();
                        if (amountMax == 0)
                            amountMax = amount + 1;

                        if (amount >= amountMin && amount <= amountMax) {

                            double feeAmount = amount * fee.getFeePercentage();
                            //long fAmount = (long) feeAmount;
                            holder.edtFee.setText(AmountFormatter.formatFeePercent(fee.getFeePercentage()) + " | " + AmountFormatter.formatNonCurrency(feeAmount));
                            Double total = amount + (amount * fee.getFeePercentage());
                            holder.edtTotal.setText(AmountFormatter.formatCurrencyWithoutComma(total));
                            holder.total = total;
                        }
                    }
                }
            }

        }


        holder.edtAmount.addTextChangedListener(new TextWatcher(){

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3){

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count){

                if (!s.toString().equalsIgnoreCase("")){

                    for(PackageListReguler packageListReguler : packageListRegulerAll){
                        if(holder.packageId == packageListReguler.getId()){
                            for(int i = 0; i < packageListReguler.getSubscriptionFeeList().size(); i++){

                                Long amount = Long.parseLong(s.toString());
                                SubscriptionFee fee = packageListReguler.getSubscriptionFeeList().get(i);
                                Long amountMin = fee.getAmountMin().longValue();
                                Long amountMax = fee.getAmountMax().longValue();
                                if (amountMax == 0)
                                    amountMax = amount + 1;

                                if (amount >= amountMin && amount <= amountMax){

                                    double feeAmount = amount * fee.getFeePercentage();
                                    //long fAmount = (long) feeAmount;
                                    holder.edtFee.setText(AmountFormatter.formatFeePercent(fee.getFeePercentage()) + " | " + AmountFormatter.formatNonCurrency(feeAmount));
                                    //holder.edtFee.setText(BigDecimal.valueOf(fee.getFeePercentage() * 100).toPlainString() + "%" + " | " + AmountFormatter.formatWithoutIdr(feeAmount));
                                    Double total = amount + (amount * fee.getFeePercentage());
                                    holder.edtTotal.setText(AmountFormatter.formatCurrencyWithoutComma(total));
                                    holder.total = total;
                                }
                            }
                        }
                    }

                    /*PackageListReguler objItem = (PackageListReguler) holder.itemView.getTag();
                    for (int i = 0; i < objItem.getSubscriptionFeeList().size(); i++) {

                        Long amount = Long.parseLong(s.toString());
                        SubscriptionFee fee = objItem.getSubscriptionFeeList().get(i);
                        Long amountMin = fee.getAmountMin().longValue();
                        Long amountMax = fee.getAmountMax().longValue();
                        if (amountMax == 0)
                            amountMax = amount + 1;

                        if (amount >= amountMin && amount <= amountMax) {
                            holder.edtFee.setText(BigDecimal.valueOf(fee.getFeePercentage() * 100).toPlainString() + "%");
                            Double total = amount + (amount * fee.getFeePercentage());
                            holder.edtTotal.setText(AmountFormatter.format(total));
                            holder.total = total;
                        }
                    }*/

                }else{
                    /*holder.edtAmount.setText("0");*/
                    holder.edtFee.setText("0%");
                    holder.edtTotal.setText("0");
                }
            }

            @Override
            public void afterTextChanged(Editable editable){

            }
        });


        holder.rlRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePackageList(position);
            }
        });

        holder.rlProspektus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

               if(holder.packageId != 0){
                    String name = listPackages.get(holder.sSelectReksaDana.getSelectedItemPosition()).getName();
                    getProspektusKey(holder.packageId);
                    donwloadDataProspektus(getProspektusKey(holder.packageId), name);
                }

            }
        });

        holderList.add(holder);
    }



    public List<ListPackage> getListPackage(List<PackageListReguler> packageListRegulerAll){
        listPackages = new ArrayList<>();
        for(PackageListReguler packageListReguler : packageListRegulerAll){
            ListPackage listPackage = new ListPackage();
            listPackage.setId(packageListReguler.getId());
            listPackage.setName(packageListReguler.getName());
            listPackages.add(listPackage);
        }
        return listPackages;
    }



    public void setupSpinnerPackage(Spinner s, List<ListPackage> listPackages, String selectionPackageId){
        int idPackage;

        if (listPackages == null) listPackages = new ArrayList<>();
        ListPackage defaultPackage = new ListPackage();
        defaultPackage.setName("");
        defaultPackage.setId(0);
        listPackages.add(0, defaultPackage);

//            for (int i = 0; i < listPackages.size() ; i++) {
//                for (PackageListReguler regular : packageListReguler) {
//                    if (listPackages.get(i).getId() != 0 && !listPackages.get(i).getId().toString().equals(selectionPackageId)
//                            && (listPackages.get(i).getId() == regular.getId())) {
//                       listPackages.remove(i);
//                    }
//                }
//            }

        ArrayAdapter<ListPackage> spinnerArrayAdapter = new ArrayAdapter<>(context, R.layout.spinner, listPackages);
        s.setAdapter(spinnerArrayAdapter);
        s.setSelection(0, false);
        if (!selectionPackageId.equalsIgnoreCase("null")){ // set selection to saved data from realm / ws
            int i = 0;
            for(ListPackage package1 : listPackages){
                if(!selectionPackageId.equals("")){
                    idPackage = Integer.valueOf(selectionPackageId);
                    if(package1.getId() == idPackage){
                        s.setSelection(i, false);
                        break;
                    }
                    i++;
                }
            }
        }
    }



    protected void setupSpinner(Spinner s, List<KycLookup> kycLookupList){
        ArrayAdapter<KycLookup> spinnerArrayAdapter = new ArrayAdapter<>(context, R.layout.spinner, kycLookupList);
        s.setAdapter(spinnerArrayAdapter);
        s.setSelection(0, false);
    }

    protected void setupSpinnerWithSpecificLookupSelection(Spinner s, List<KycLookup> kycLookupList, String lookupCode) {
        setupSpinner(s, kycLookupList);
        int i = 0;
        for (KycLookup lookup : kycLookupList){
            if (lookup.getValue().equalsIgnoreCase(lookupCode)){
                s.setSelection(i, false);
                break;
            }
            i++;
        }
    }


    public List<PackageListReguler> getList(){

        for (int i = 0; i < packageListReguler.size(); i++){

            PackageListReguler it = packageListReguler.get(i);
            SelectReksaDanaListAdapter.SelectReksaDanaHolder ho = holderList.get(i);
            it.setTransactionAmount(String.valueOf(ho.edtAmount.getText()));
            it.setFeePercentage(ho.edtFee.getText().toString());
            it.setName(ho.sSelectReksaDana.getSelectedItem().toString());

            if(!ho.edtAmount.getText().toString().equals("0") && !ho.edtAmount.getText().toString().equals("") && ho.total != null){
                it.setFeePrice(ho.total - Double.parseDouble(ho.edtAmount.getText().toString()));
            }else{
                it.setFeePrice(0.0);
            }

            if(ho.total != null){
                BigDecimal totalConvert = BigDecimal.valueOf(ho.total);
                it.setTotal(totalConvert.toString());
            }else{
                it.setTotal("0");
            }

            if(ho.minSubcription != 0.0){
                it.setMinSubscription(ho.minSubcription);
            }else{
                it.setMinSubscription(0.0);
            }

            if(ho.packageId != 0){
                it.setId(ho.packageId);
            }else{
                it.setId(0);
            }

            if(!ho.timePeriod.equals("")){
                it.setTimePeriod(ho.timePeriod);
            }else{
                it.setTimePeriod("");
            }

            if(ho.cbAccept.isChecked()){
                it.setAccept(true);
            }else{
                it.setAccept(false);
            }

            packageListReguler.set(i, it);
        }

        return packageListReguler;
    }





    @Override
    public int getItemCount(){
        return packageListReguler != null ? packageListReguler.size() : 0;
    }


    public static class SelectReksaDanaHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.sSelectReksaDana)
        Spinner sSelectReksaDana;
        @Bind(R.id.cbAccept)
        CheckBox cbAccept;
        @Bind(R.id.sTimePeriod)
        Spinner sTimePeriod;
        @Bind(R.id.edtAmount)
        EditText edtAmount;
        @Bind(R.id.edtFee)
        EditText edtFee;
        @Bind(R.id.edtTotal)
        EditText edtTotal;
        @Bind(R.id.rlProspektus)
        RelativeLayout rlProspektus;
        @Bind(R.id.rlRemove)
        RelativeLayout rlRemove;
        private Double total;
        private double minSubcription;
        private int packageId;
        private String timePeriod;
        private boolean accept;

        public SelectReksaDanaHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }



    public interface CallbackPackage {
        void removePackage(int index);
        void downloadProspektus(String prospektusKey, String packageName);
    }

    private void removePackageList(int index){
        mCallback.removePackage(index);
    }

    private void donwloadDataProspektus(String prospektusKey, String packageName){
        mCallback.downloadProspektus(prospektusKey, packageName);
    }


    private String getProspektusKey(int id){
        String prospektusKey = "";
        for(int i = 0; i < packageListRegulerAll.size(); i++){
            if(packageListRegulerAll.get(i).getId() == id){
                prospektusKey = packageListRegulerAll.get(i).getProspectusKey();
                break;
            }
        }
        return prospektusKey;
    }





}
