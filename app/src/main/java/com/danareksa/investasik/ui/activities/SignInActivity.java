package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.fragments.signIn.SignInFragment;
import com.danareksa.investasik.util.FontsOverride;

import butterknife.Bind;

/**
 * Created by fajarfatur on 1/12/16.
 */
public class SignInActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    private static final String AUTO_LOGOUT = "autoLogout";

    int click = 0;

    public static void startActivity(BaseActivity sourceActivity){
        FragmentManager manager = sourceActivity.getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry first = manager.getBackStackEntryAt(0);
            manager.popBackStack(first.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        Intent intent = new Intent(sourceActivity, SignInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sourceActivity.startActivity(intent);
    }

    public static void startActivityFromAutoLogout(BaseActivity sourceActivity) {
        Intent intent = new Intent(sourceActivity, SignInActivity.class);
        intent.putExtra(AUTO_LOGOUT, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        sourceActivity.startActivityForResult(intent, 0);
    }


    @Override
    protected int getLayout() {
        return R.layout.a_signin;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setupToolbar();

        HIDE_CART = false;
        IS_FROM_LANDING = false;

        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/HelveticaNeueLts.otf");

        if (BuildConfig.FLAVOR.contentEquals("dim") || BuildConfig.FLAVOR.contentEquals("btn")){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                        WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            }
        }

        SignInFragment.showFragment(this);
        if(getIntent().hasExtra(AUTO_LOGOUT) && getIntent().getBooleanExtra(AUTO_LOGOUT, false)){
            showAutoLogoutDialog();
        }
    }


    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        title.setText("Masuk");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private void showAutoLogoutDialog() {

        new MaterialDialog.Builder(this)
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.auto_logout).toUpperCase())
                .titleColor(Color.BLACK)
                .content(R.string.expired_session)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.BLUE)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);/*//***Change Here***
        startActivity(intent);
        finish();
        LandingPageActivity.startActivity(this);
        System.exit(0);*/

        startActivity(new Intent(SignInActivity.this, LandingPageActivity.class));
        finish();
    }


    public void setActionBarTitle(String title){
        getSupportActionBar().setTitle(title);
    }


}
