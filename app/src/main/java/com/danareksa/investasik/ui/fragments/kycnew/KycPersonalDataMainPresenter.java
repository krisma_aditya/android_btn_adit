package com.danareksa.investasik.ui.fragments.kycnew;

import android.widget.Toast;

import com.danareksa.investasik.data.api.beans.Bank;
import com.danareksa.investasik.data.api.beans.City;
import com.danareksa.investasik.data.api.beans.Country;
import com.danareksa.investasik.data.api.beans.Kyc;
import com.danareksa.investasik.data.api.beans.State;
import com.danareksa.investasik.data.api.requests.CountryRequest;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.api.requests.KycLookupRequest;
import com.danareksa.investasik.data.api.responses.KycLookupResponse;
import com.danareksa.investasik.data.api.responses.LoadKycDataResponse;
import com.danareksa.investasik.data.api.responses.SaveDataKycResponse;
import com.danareksa.investasik.data.api.responses.StatusCustomerResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.data.realm.RealmHelper;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.KycFinishActivity;
import com.danareksa.investasik.util.Constant;
import com.google.gson.Gson;

import org.modelmapper.ModelMapper;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by asep.surahman on 05/07/2018.
 */

public class KycPersonalDataMainPresenter {

    private static final String MAX = "999";
    private static final String OFFSET = "0";

    KycPersonalDataMain fragment;

    public KycPersonalDataMainPresenter(KycPersonalDataMain fragment){
        this.fragment = fragment;
    }


    void getKycLookupData(){
        fragment.showProgressBar();
        if (!isKycLookupAlreadyOnRealm()){
            fragment.getApi().getKycLookupNew(getKycLookupRequest())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Observer<KycLookupResponse>(){
                        @Override
                        public void onCompleted(){
                        }

                        @Override
                        public void onError(Throwable e){
                            fragment.dismissProgressBar();
                            fragment.connectionError();
                            fragment.isErrorLoadKyc = true;
                        }

                        @Override
                        public void onNext(KycLookupResponse kycLookupResponse){
                            fragment.isErrorLoadKyc = false;
                            RealmHelper.createKycLookup(fragment.getRealm(), kycLookupResponse.getKycLookupList());
                            getKycDataFromServer();
                        }
                    });
        }else{
            getKycDataFromServer();
        }
    }



    void getKycDataFromServer(){
        fragment.getApi().loadKycDataNew(getKycDataRequest())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<LoadKycDataResponse>() {
                    @Override
                    public void onCompleted(){
                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.dismissProgressBar();
                        fragment.connectionError();
                        fragment.isErrorLoadKyc = true;
                    }

                    @Override
                    public void onNext(LoadKycDataResponse loadKycDataResponse) {
                        fragment.isErrorLoadKyc = false;
                        fragment.loadKycDataResponse = loadKycDataResponse;
                        initiateKycDataRequest(loadKycDataResponse.getData());
                        getAllBank();
                    }
                });
    }




    public void getAllBank(){
        fragment.getApi().getAllBank(MAX, OFFSET, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Bank>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                        fragment.isErrorLoadKyc = true;
                    }

                    @Override
                    public void onNext(List<Bank> banks){
                        fragment.banks = banks;
                        fragment.isErrorLoadKyc = false;
                        if (banks != null && banks.size() > 0) {
                            getAllCountry();
                        }else{
                            getAllCountry();
                        }
                    }
                });
    }


    
    public void getAllCountry(){
        fragment.getApi().getAllCountryNew(getCountryRequest())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Country>>(){
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                        fragment.isErrorLoadKyc = true;
                    }

                    @Override
                    public void onNext(List<Country> countries) {
                        fragment.countries = countries;
                        fragment.isErrorLoadKyc = false;
                        if (countries != null && countries.size() > 0){
                            String countryId = fragment.loadKycDataResponse.getData().getLegalCountry();
                            if (countryId != null) {
                                //getState(String.valueOf(countryId), countries);
                                getState(Constant.ID_COUNTRY_INDO, countries);
                            } else {
                                getState(Constant.ID_COUNTRY_INDO, countries);
                            }

                        } else {
                            String countryId = fragment.loadKycDataResponse.getData().getLegalCountry();
                            getState(Constant.ID_COUNTRY_INDO, countries);
                        }
                        //getState("52", countries);
                    }
                });
    }



    public void getState(String countryId, final List<Country> countries){
        fragment.getApi().getStateByCountry(countryId, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<State>>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                        fragment.isErrorLoadKyc = true;
                    }

                    @Override
                    public void onNext(List<State> states) {
                        fragment.isErrorLoadKyc = false;
                        if (states.size() > 0) {
                            String provinceId = fragment.loadKycDataResponse.getData().getHomeProvince();

                            if (provinceId != null){
                                getCity(provinceId, countries, states);
                            }
                            getCity("ID-JK", countries, states);
                        } else {
                            String provinceId = fragment.loadKycDataResponse.getData().getHomeProvince();
                            getCity(provinceId, countries, states);
                        }
                        //getCity("ID-JK", countries, states);

                    }
                });
    }



    public void getCity(String stateId, final List<Country> countries, final List<State> states) {
        fragment.getApi().getCityByState(stateId, PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<City>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                        fragment.isErrorLoadKyc = true;
                    }

                    @Override
                    public void onNext(List<City> cities) {
                        fragment.dismissProgressDialog();
                        fragment.isErrorLoadKyc = false;
                        String jsonCountry = new Gson().toJson(countries);
                        String jsonState = new Gson().toJson(states);
                        String jsonCity = new Gson().toJson(cities);
                        if (cities.size() > 0) {
                            fragment.setUpAdapter(jsonCountry, jsonState, jsonCity);
                            fragment.dismissProgressBar();
                        } else {
                            fragment.setUpAdapter(jsonCountry, jsonState, jsonCity);
                            fragment.dismissProgressBar();
                        }
                        //fragment.endThread();
                    }
                });
    }




    private KycDataRequest requestSubmitKycDataNew(KycDataRequest request){
        Timber.i("Submitted KYC Data To WS: %s", request);
        KycDataRequest kycDataRequest = new KycDataRequest();
        kycDataRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        kycDataRequest.setCitizenship(request.getCitizenship());
        kycDataRequest.setIdType(request.getIdType());
        kycDataRequest.setIdNumber(request.getIdNumber());
        kycDataRequest.setTaxId(request.getTaxId());
        kycDataRequest.setBirthDate(request.getBirthDate());
        kycDataRequest.setNationality(request.getNationality());
        kycDataRequest.setIdImage(request.getIdImage());
        kycDataRequest.setSelfieImage(request.getSelfieImage());
        kycDataRequest.setTaxIdImage(request.getTaxIdImage());
        kycDataRequest.setGender(request.getGender());
        kycDataRequest.setBirthPlace(request.getBirthPlace());
        kycDataRequest.setMaritalStatus(request.getMaritalStatus());
        kycDataRequest.setReligion(request.getReligion());
        kycDataRequest.setMotherName(request.getMotherName());
        kycDataRequest.setEducation(request.getEducation());
        kycDataRequest.setAnnualIncome(request.getAnnualIncome());
        kycDataRequest.setFundSource(request.getFundSource());
        kycDataRequest.setInvestmentPurpose(request.getInvestmentPurpose());
        kycDataRequest.setIdAddress(request.getIdAddress());
        kycDataRequest.setIdCountry(request.getIdCountry());
        kycDataRequest.setIdProvince(request.getIdProvince());
        kycDataRequest.setIdCity(request.getIdCity());
        kycDataRequest.setIdPostalCode(request.getIdPostalCode());
        kycDataRequest.setOccupation(request.getOccupation());
        kycDataRequest.setEmployer(request.getEmployer());
        kycDataRequest.setJobPosition(request.getJobPosition());
        kycDataRequest.setOfficeAddress(request.getOfficeAddress());
        kycDataRequest.setOfficeCountry(request.getOfficeCountry());
        kycDataRequest.setOfficeProvince(request.getOfficeProvince());
        kycDataRequest.setOfficeCity(request.getOfficeCity());
        kycDataRequest.setOfficePostalCode(request.getOfficePostalCode());
        kycDataRequest.setAdditionalIncome(request.getAdditionalIncome());
        kycDataRequest.setAdditionalFundSource(request.getAdditionalFundSource());
        kycDataRequest.setMailingAddressType(request.getMailingAddressType());
        kycDataRequest.setMailingAddress(request.getMailingAddress());
        kycDataRequest.setMailingCountry(request.getMailingCountry());
        kycDataRequest.setMailingProvince(request.getMailingProvince());
        kycDataRequest.setMailingCity(request.getMailingCity());
        kycDataRequest.setMailingPostalCode(request.getMailingPostalCode());
        kycDataRequest.setFundSourceName(request.getFundSourceName());
        kycDataRequest.setFundSourceRelationship(request.getFundSourceRelationship());
        kycDataRequest.setFundSourceIdType(request.getFundSourceIdType());
        kycDataRequest.setFundSourceIdNumber(request.getFundSourceIdNumber());
        kycDataRequest.setFundSourceOccupation(request.getFundSourceOccupation());
        kycDataRequest.setFundSourceEmployer(request.getFundSourceEmployer());
        kycDataRequest.setFundSourceOfFundSource(request.getFundSourceOfFundSource());
        kycDataRequest.setBeneficiaryType(request.getBeneficiaryType());
        kycDataRequest.setBeneficiaryName(request.getBeneficiaryName());
        kycDataRequest.setBeneficiaryRelationship(request.getBeneficiaryRelationship());
        kycDataRequest.setBeneficiaryIdType(request.getBeneficiaryIdType());
        kycDataRequest.setBeneficiaryIdNumber(request.getBeneficiaryIdNumber());
        kycDataRequest.setBeneficiaryOccupation(request.getBeneficiaryOccupation());
        kycDataRequest.setBeneficiaryEmployer(request.getBeneficiaryEmployer());
        kycDataRequest.setBeneficiaryFundSource(request.getBeneficiaryFundSource());
        kycDataRequest.setBeneficiaryAnnualIncome(request.getBeneficiaryAnnualIncome());
        kycDataRequest.setBeneficiaryRelationshipOther(request.getBeneficiaryRelationshipOther());
        kycDataRequest.setHeirName(request.getHeirName());
        kycDataRequest.setHeirMobileNumber(request.getHeirMobileNumber());
        kycDataRequest.setHeirRelationship(request.getHeirRelationship());
        kycDataRequest.setHeirRelationshipOther(request.getHeirRelationshipOther());
        kycDataRequest.setHeirInvestmentGoal(request.getHeirInvestmentGoal());
        kycDataRequest.setBankAccountLists(request.getBankAccountLists());
        kycDataRequest.setRiskQuestionnaire(request.getRiskQuestionnaire());
        kycDataRequest.setAppsType("ANDROID");
        return kycDataRequest;
    }


    void saveDataKyc(KycDataRequest kycDataRequest, Boolean showInfo){
        fragment.showProgressBar();
        fragment.getApi().saveKycDataNew(requestSubmitKycDataNew(kycDataRequest))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<SaveDataKycResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(SaveDataKycResponse saveDataKycResponse){
                        fragment.dismissProgressBar();
                        if(saveDataKycResponse.getCode() == 0){
                            getCustomerStatus();
                            if(saveDataKycResponse.getData().getCompleteness() == 100){
                                fragment.showDialogSubmit(saveDataKycResponse.getInfo());
                            }else{
                                fragment.showDialogAfterSubmit(saveDataKycResponse.getInfo(), showInfo);
                            }
                        }else{
                            Toast.makeText(fragment.getActivity(), saveDataKycResponse.getInfo(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }


    private boolean isKycLookupAlreadyOnRealm(){
        return RealmHelper.getKycLookupSize(fragment.getRealmKyc()) > 0;
    }


    private void initiateKycDataRequest(Kyc kyc){
        if (kyc != null) {
            ModelMapper modelMapper = new ModelMapper();
            fragment.request = modelMapper.map(kyc, KycDataRequest.class);
        } else {
            fragment.request = new KycDataRequest();
        }
    }


    private CountryRequest getCountryRequest(){
        CountryRequest countryRequest = new CountryRequest();
        countryRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        return countryRequest;
    }


    private KycDataRequest getKycDataRequest(){
        KycDataRequest kycDataRequest = new KycDataRequest();
        kycDataRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        return kycDataRequest;
    }


    private KycLookupRequest getKycLookupRequest(){
        KycLookupRequest kycLookupRequest = new KycLookupRequest();
        kycLookupRequest.setToken(PrefHelper.getString(PrefKey.TOKEN));
        return kycLookupRequest;
    }




    void saveAndSubmitKyc(KycDataRequest kycDataRequest, final String sourceRoute){
        fragment.showProgressBar();
        fragment.getApi().saveKycDataNew(requestSubmitKycDataNew(kycDataRequest))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<SaveDataKycResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e){
                        fragment.connectionError();
                        fragment.dismissProgressBar();
                    }

                    @Override
                    public void onNext(SaveDataKycResponse saveDataKycResponse){
                        fragment.dismissProgressBar();
                        if(saveDataKycResponse.getCode() == 0){
                            if(saveDataKycResponse.getData().getCompleteness() == 100){
                                fragment.getActivity().finish();
                                KycFinishActivity.startActivity((BaseActivity) fragment.getActivity());
                            }else{
                                fragment.showDialogAfterSubmit(saveDataKycResponse.getInfo(), true);
                            }

                            getCustomerStatus();

                        }else{
                            Toast.makeText(fragment.getActivity(), saveDataKycResponse.getInfo(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }




    void getCustomerStatus(){
        fragment.getApi().getStatusCustomer(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<StatusCustomerResponse>() {
                    @Override
                    public void onCompleted(){

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(StatusCustomerResponse statusCustomerResponse){
                        if (statusCustomerResponse.getCode() == 0){
                            if(statusCustomerResponse.getData().equalsIgnoreCase("activated")){
                                PrefHelper.setString(PrefKey.CUSTOMER_STATUS, Constant.USER_STATUS_ACTIVE);
                            }else if(statusCustomerResponse.getData().equalsIgnoreCase("pending")){
                                PrefHelper.setString(PrefKey.CUSTOMER_STATUS, Constant.USER_STATUS_PENDING);
                            }else if(statusCustomerResponse.getData().equalsIgnoreCase("verified")){
                                PrefHelper.setString(PrefKey.CUSTOMER_STATUS, Constant.USER_STATUS_VERIFIED);
                            }
                        }
                    }
                });
    }







}
