package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.InvestmentAccountInfo;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.ListOfRegulerPurchaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by asep.surahman on 22/05/2018.
 */

public class CategoryPortfolioAdapter extends RecyclerView.Adapter<CategoryPortfolioAdapter.CategoryPortfolioHolder> {

    private Context context;
    //private List<CategoryPortfolio> list;
    Packages packages;
    private List<InvestmentAccountInfo> list = new ArrayList<>();

    public CategoryPortfolioAdapter(Context context, List<InvestmentAccountInfo> list, Packages packages) {
        this.context = context;
        this.list = list;
        this.packages = packages;
    }

    @Override
    public CategoryPortfolioAdapter.CategoryPortfolioHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_category_portfolio, parent, false);
        return new CategoryPortfolioAdapter.CategoryPortfolioHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CategoryPortfolioAdapter.CategoryPortfolioHolder holder, final int position) {
        final InvestmentAccountInfo item = list.get(position);
        holder.itemView.setTag(item);
        holder.tvCategoryId.setText(item.getIfua());
        holder.tvCategoryName.setText(item.getInvestmentGoal());
        holder.llCategoryPortfolio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                ListOfRegulerPurchaseActivity.startActivity((BaseActivity) context, item.getIfua(), packages);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class CategoryPortfolioHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.tvCategoryId)
        TextView tvCategoryId;
        @Bind(R.id.tvCategoryName)
        TextView tvCategoryName;
        @Bind(R.id.llCategoryPortfolio)
        LinearLayout llCategoryPortfolio;

        public CategoryPortfolioHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
