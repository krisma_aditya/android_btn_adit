package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.PackageDetail;
import com.danareksa.investasik.util.AmountFormatter;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by asep.surahman on 08/08/2018.
 */

public class SummaryPackageAdapter extends RecyclerView.Adapter<SummaryPackageAdapter.SummaryPackageHolder> {


    List<PackageDetail> packageList;
    private PackageDetail itemPackage;
    private Context context;
    private List<SummaryPackageAdapter.SummaryPackageHolder> holderList;


    public SummaryPackageAdapter(Context context, List<PackageDetail> packageLists) {
        this.context = context;
        this.packageList = packageLists;
        this.holderList = new ArrayList<>();
    }


    @Override
    public SummaryPackageAdapter.SummaryPackageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_summary_packages, parent, false);
        return new SummaryPackageAdapter.SummaryPackageHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final SummaryPackageAdapter.SummaryPackageHolder holder, final int position) {
        itemPackage = packageList.get(position);
        holder.itemView.setTag(itemPackage);

        holder.tvNo.setText(String.valueOf(position+1) + ". ");
        holder.tvPackageName.setText(itemPackage.getPackageName());
        holder.tvPeriod.setText(itemPackage.getRegulerPeriod() + " Bulan");

        if(itemPackage.getMonthlyInvestmentAmount() != 0.0){
            double totInv = 0;
            if(itemPackage.getMonthlyInvestmentFee() != 0.0 && itemPackage.getTotalMonthlyAutodebet() != 0.0){
                totInv = itemPackage.getTotalMonthlyAutodebet() - itemPackage.getMonthlyInvestmentFee();
            }else{
                totInv = itemPackage.getMonthlyInvestmentAmount();
            }
            holder.tvAmount.setText(String.valueOf(AmountFormatter.format(totInv)));
        }else{
            holder.tvAmount.setText("0");
        }

        if(itemPackage.getTotalMonthlyAutodebet() != 0.0){
            holder.tvTotalAmount.setText(String.valueOf(AmountFormatter.format(itemPackage.getTotalMonthlyAutodebet())));
        }else{
            holder.tvTotalAmount.setText("0");
        }

        if(itemPackage.getMonthlyInvestmentFee() != 0.0){
            holder.tvFee.setText(String.valueOf(AmountFormatter.format(itemPackage.getMonthlyInvestmentFee())));
        }else{
            holder.tvFee.setText("0");
        }
        holderList.add(holder);
    }


    @Override
    public int getItemCount() {
        return packageList != null ? packageList.size() : 0;
    }


    public static class SummaryPackageHolder extends RecyclerView.ViewHolder {


        @Bind(R.id.tvPackageName)
        TextView tvPackageName;
        @Bind(R.id.tvAmount)
        TextView tvAmount;
        @Bind(R.id.tvFee)
        TextView tvFee;
        @Bind(R.id.tvPeriod)
        TextView tvPeriod;
        @Bind(R.id.tvTotalAmount)
        TextView tvTotalAmount;
        @Bind(R.id.tvNo)
        TextView tvNo;




        public SummaryPackageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}




