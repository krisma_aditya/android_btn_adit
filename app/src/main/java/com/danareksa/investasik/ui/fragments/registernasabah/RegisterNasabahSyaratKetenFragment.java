package com.danareksa.investasik.ui.fragments.registernasabah;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.RegisterNewCustomer;
import com.danareksa.investasik.data.api.beans.RegisterOnlineAccess;
import com.danareksa.investasik.data.api.beans.TermAndCondition;
import com.danareksa.investasik.data.api.requests.RegisterNasabahRequest;
import com.danareksa.investasik.data.api.responses.TermAndConditionResponse;
import com.danareksa.investasik.ui.activities.ActivationActivity;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.LandingPageActivity;
import com.danareksa.investasik.ui.activities.RegisterNasabahActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by asep.surahman on 08/05/2018.
 */


public class RegisterNasabahSyaratKetenFragment extends BaseFragment {

    public static final String TAG = RegisterNasabahbaruFragment.class.getSimpleName();
    public static final String NASABAH_TYPE = "tipe_nasabah";
    public static final String EMAIL = "email";
    public static BaseActivity activity;

    String tipeNasabah = "";
    String email = "";

    TextView textInfo;
    Button bOke;

    Dialog dialogKonfimasi;
    LayoutInflater inflater;

    @Bind(R.id.bSignUp)
    Button bSignUp;

    @Bind(R.id.cbAgree)
    CheckBox cbAgree;

    String fullName = "";

    boolean valid;

    private static final String REGISTER_NEW_REQUEST = "registerNewModel";
    private static final String REGISTER_ONLINE_REQUEST = "registerOnlineModel";

    @State
    RegisterNewCustomer registerNewCustomer;

    @State
    RegisterOnlineAccess registerOnlineAccess;

    @State
    RegisterNasabahRequest registerNasabahRequest;

    private RegisterNasabahSyaratKetenPresenter presenter;


    public static void showFragment(BaseActivity sourceActivity,  String tipeNasabah, RegisterNewCustomer registerNewCustomer, String email){
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            activity = sourceActivity;
            Bundle bundle = new Bundle();
            bundle.putString(NASABAH_TYPE, tipeNasabah);
            bundle.putString(EMAIL, email);
            bundle.putSerializable(REGISTER_NEW_REQUEST, registerNewCustomer);
            RegisterNasabahSyaratKetenFragment fragment = new RegisterNasabahSyaratKetenFragment();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }


    public static void showFragment(BaseActivity sourceActivity,  String tipeNasabah, RegisterOnlineAccess registerOnlineAccess){
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            activity = sourceActivity;
            Bundle bundle = new Bundle();
            bundle.putString(NASABAH_TYPE, tipeNasabah);
            bundle.putSerializable(REGISTER_ONLINE_REQUEST, registerOnlineAccess);
            RegisterNasabahSyaratKetenFragment fragment = new RegisterNasabahSyaratKetenFragment();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }

    public static void showFragment(BaseActivity sourceActivity,  String tipeNasabah, RegisterNasabahRequest registerNasabahRequest){
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            activity = sourceActivity;
            Bundle bundle = new Bundle();
            bundle.putString(NASABAH_TYPE, tipeNasabah);
            bundle.putSerializable(REGISTER_ONLINE_REQUEST, registerNasabahRequest);
            RegisterNasabahSyaratKetenFragment fragment = new RegisterNasabahSyaratKetenFragment();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected int getLayout(){
        return R.layout.f_register_nasabah_syarat_ketentuan;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new RegisterNasabahSyaratKetenPresenter(this);
        tipeNasabah = getArguments().getString(NASABAH_TYPE);
        email = getArguments().getString(EMAIL);

        if(getArguments().getSerializable(REGISTER_NEW_REQUEST) != null){
            registerNewCustomer = (RegisterNewCustomer) getArguments().getSerializable(REGISTER_NEW_REQUEST);
        }

        if(getArguments().getSerializable(REGISTER_ONLINE_REQUEST) !=  null){
//            registerOnlineAccess = (RegisterOnlineAccess) getArguments().getSerializable(REGISTER_ONLINE_REQUEST);
                if(BuildConfig.FLAVOR.contentEquals("btn")){
                    registerNasabahRequest = (RegisterNasabahRequest) getArguments().getSerializable(REGISTER_ONLINE_REQUEST);
                } else {
                    registerOnlineAccess = (RegisterOnlineAccess) getArguments().getSerializable(REGISTER_ONLINE_REQUEST);                }
            }


        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bSignUp.setEnabled(false);
        getTermAndCondition(tipeNasabah);

        cbAgree.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    bSignUp.setEnabled(true);
                } else {
                    bSignUp.setEnabled(false);
                }
            }
        });
    }


    @Override
    public void onResume(){
        super.onResume();
    }


    @OnClick(R.id.bSignUp)
    void bDaftar(){
        valid = cbAgree.isChecked();
        if(valid == true){
            if(tipeNasabah.equalsIgnoreCase("baru")){
                presenter.register(presenter.constructRegisterNewCustomerRequest(registerNewCustomer), email);
            }else if(tipeNasabah.equalsIgnoreCase("lama")){
//                presenter.registerOnlineAccess(presenter.constructRegisterOnlineAccessRequest(registerOnlineAccess));
//                presenter.registerNasabah(presenter.constructRegisterNasabahRequest(registerNasabahRequest));
                if(BuildConfig.FLAVOR.contentEquals("btn")){
                    presenter.registerNasabah(presenter.constructRegisterNasabahRequest(registerNasabahRequest));
                } else {
                    presenter.registerOnlineAccess(presenter.constructRegisterOnlineAccessRequest(registerOnlineAccess));
                }
            }
        }else{
            showDialog("Silahkan checklist syarat dan ketentuan berlaku");
        }

    }


    public void showDialogKonfirmasi(){
        dialogKonfimasi = new Dialog(getActivity());
        View view = inflater.inflate(R.layout.popup_syarat_ketentuan, null);
        textInfo  = (TextView) view.findViewById(R.id.textInfo);
        bOke      = (Button)   view.findViewById(R.id.btnOk);
        dialogKonfimasi.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogKonfimasi.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#50000000")));
        dialogKonfimasi.setContentView(view);
        dialogKonfimasi.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialogKonfimasi.show();
        bOke.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialogKonfimasi.dismiss();
                LandingPageActivity.startActivity(activity);
                getActivity().finish();
                RegisterNasabahActivity.regNasabahActivity.finish();
            }
        });
    }


    public void gotoActivationCodeActivity(String username, String password, String email) {
        ActivationActivity.startActivity((BaseActivity) getActivity(), username, password, email);
        getActivity().finish();
        RegisterNasabahActivity.regNasabahActivity.finish();
    }

    public void getTermAndCondition(String type){

        if (type.equalsIgnoreCase("baru")) {
            this.getApi().getTermAndConditionNewCustomer()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Observer<TermAndConditionResponse>() {

                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable throwable) {

                        }

                        @Override
                        public void onNext(TermAndConditionResponse termAndConditionResponse) {

                            setContent(termAndConditionResponse.getData());
                        }
                    });
        } else {
            this.getApi().getTermAndConditionExistingCustomer()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Observer<TermAndConditionResponse>() {

                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable throwable) {

                        }

                        @Override
                        public void onNext(TermAndConditionResponse termAndConditionResponse) {

                            setContent(termAndConditionResponse.getData());
                        }
                    });
        }

    }

    static void setContent(TermAndCondition termAndCondition) {
        WebView wv1 = (WebView) activity.findViewById(R.id.webView);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.getSettings().setDomStorageEnabled(true);
        wv1.loadData(termAndCondition.getTerms(),"text/html", null);
    }


    void showDialog(String info){
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(getString(R.string.infortmation).toUpperCase())
                .titleColor(Color.BLACK)
                .content(info)
                .contentColor(Color.GRAY)
                .positiveText(R.string.ok)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }





}
