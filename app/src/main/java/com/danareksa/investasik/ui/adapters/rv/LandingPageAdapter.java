package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.Slideshow;
import com.squareup.picasso.Picasso;

import org.sufficientlysecure.htmltextview.HtmlHttpImageGetter;
import org.sufficientlysecure.htmltextview.HtmlTextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by asep.surahman on 28/03/2018.
 */

public class LandingPageAdapter extends RecyclerView.Adapter<LandingPageAdapter.LandingPageHolder>{

    private Context context;
    private List<Slideshow> list;

    public LandingPageAdapter(Context context, List<Slideshow> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public LandingPageAdapter.LandingPageHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_landing_page, parent, false);
        LandingPageAdapter.LandingPageHolder landingPageHolder = new LandingPageAdapter.LandingPageHolder(itemView);
        return landingPageHolder;
    }

    @Override
    public void onBindViewHolder(LandingPageAdapter.LandingPageHolder holder, int position){
        final Slideshow item = list.get(position);
        holder.itemView.setTag(item);
        holder.tvTitle.setText(item.getTitle());
        /*holder.tvContent.setText(item.getDescription());*/
        String cap = item.getType().substring(0, 1).toUpperCase() + item.getType().substring(1).toLowerCase();
        holder.tvCategory.setText(cap);

        Picasso.with(context).load(InviseeService.IMAGE_DOWNLOAD_URL + item.getImage())
                .placeholder(R.drawable.ic_logo_launcher)
                .error(R.drawable.ic_logo_launcher)
                .into(holder.ivLanding);

        holder.html_text.setHtml(item.getDescription(), new HtmlHttpImageGetter(holder.html_text));
        holder.ivLanding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //DetailPromoActivity.startActivity((BaseActivity) context, item);
            }
        });
    }


    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public static class LandingPageHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ivLanding)
        CircleImageView ivLanding;

        @Bind(R.id.tvTitle)
        TextView tvTitle;

     /*   @Bind(R.id.tvContent)
        TextView tvContent;*/

        @Bind(R.id.tvCategory)
        TextView tvCategory;

        @Bind(R.id.html_text)
        HtmlTextView html_text;

        public LandingPageHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }



}
