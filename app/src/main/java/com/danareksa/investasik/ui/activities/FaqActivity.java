package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.fragments.support.SupportFragment;

import butterknife.Bind;

/**
 * Created by pandu.abbiyuarsyah on 08/03/2017.
 */

public class FaqActivity extends BaseActivity {

//    private static final String FAQ = "faq";

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

//    @State
//    FaqResponse response;

    @Override
    protected int getLayout() {
        return R.layout.a_faq;
    }

    public static void startActivity(BaseActivity sourcectivity) {
        Intent intent = new Intent(sourcectivity, FaqActivity.class);
//        intent.putExtra(FAQ, response);
        sourcectivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar();
//        response = (FaqResponse) getIntent().getSerializableExtra(FAQ);
        SupportFragment.showFragment(this);
    }

    public void setupToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(null);
        title.setText("F.A.Q");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
//        startActivity(new Intent(FaqActivity.this, MainActivity.class));
//        finish();
        if (PrefHelper.getBoolean(PrefKey.IS_LOGIN)) {
            startActivity(new Intent(FaqActivity.this, MainActivity.class));
        } else {
            startActivity(new Intent(FaqActivity.this, LandingPageActivity.class));
        }
        finish();
    }

}
