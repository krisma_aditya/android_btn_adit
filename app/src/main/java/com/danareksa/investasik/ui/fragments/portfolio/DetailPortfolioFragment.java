package com.danareksa.investasik.ui.fragments.portfolio;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.danareksa.investasik.data.api.responses.FundAllocationResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.PortfolioConfirmationActivity;
import com.danareksa.investasik.ui.activities.RedemptionActivity;
import com.danareksa.investasik.ui.adapters.pager.DetailPortfolioAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.ui.NonSwipeableViewPager;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by fajarfatur on 3/1/16.
 */
public class DetailPortfolioFragment extends BaseFragment {

    public static final String TAG = DetailPortfolioFragment.class.getSimpleName();
    private static final String PORTFOLIO = "portfolio";
    private static final String IFUA_ID = "ifuaId";
    private static final String ACCOUNT_TYPE = "accountType";


/*    @Bind(R.id.tabs)
    TabLayout tabs;*/
    @Bind(R.id.pager)
    NonSwipeableViewPager pager;
    @Bind(R.id.lnTopUp)
    Button lnTopUp;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;

    @Bind(R.id.bPerformaInvest)
    Button bPerformaInvest;
    @Bind(R.id.bDetail)
    Button bDetail;

    @State
    int pageNumber = 0;
    String ifuaId = "";
    String accountType = "";
    boolean statusSpecialFee = false;
    double specialFee = 0.0;
    Integer totalDays = 0;

    public PortfolioInvestment investment;
    public Packages packages;
    public FundAllocationResponse fundAlloc;
    private DetailPortfolioAdapter detailPortfolioAdapter;

    int status = 0;

    public static void showFragment(BaseActivity sourceActivity, PortfolioInvestment investment, String ifuaId, String accountType) {

        if (!sourceActivity.isFragmentNotNull(TAG)){
            Bundle bundle = new Bundle();
            bundle.putSerializable(PORTFOLIO, investment);
            bundle.putString(IFUA_ID, ifuaId);
            bundle.putString(ACCOUNT_TYPE, accountType);
            DetailPortfolioFragment fragment = new DetailPortfolioFragment();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, fragment, TAG);
            fragmentTransaction.commit();
        }
    }

    private DetailPortfolioPresenter presenter;

    @Override
    protected int getLayout() {
        return R.layout.f_detail_portfolio;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        investment = (PortfolioInvestment) getArguments().get(PORTFOLIO);
        ifuaId = getActivity().getIntent().getStringExtra(IFUA_ID);
        accountType = getActivity().getIntent().getStringExtra(ACCOUNT_TYPE);
        presenter = new DetailPortfolioPresenter(this);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        presenter.packageDetail(investment);
        status = 0;
    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.cartList();
    }


    public void setupViewPager() {
        detailPortfolioAdapter = new DetailPortfolioAdapter(getFragmentManager(), investment, packages, ifuaId);
        pager.setAdapter(detailPortfolioAdapter);
    }


    @OnClick(R.id.lnRedemption)
    void redemption(){
        presenter.checkRedemptionTransaction(investment.getInvestmentAccountNumber());
    }


    @OnClick(R.id.lnTopUp)
    void topUp(){
        presenter.topUpValidation(investment.getInvestmentAccountNumber());
    }

    void redemptionGranted(){
        RedemptionActivity.startActivity((BaseActivity) getActivity(), investment, packages, ifuaId, statusSpecialFee, specialFee, totalDays);
        //RedemptionActivity.startActivity((BaseActivity) getActivity(), investment, packages, ifuaId, true, 0.05);
    }


    void portfolioConfirmation(){
        PortfolioConfirmationActivity.startActivity((BaseActivity) getActivity(), investment, packages, fundAlloc, ifuaId);
    }


    public void showConfirmation(String info) {
        new MaterialDialog.Builder(getActivity())
                .iconRes(R.mipmap.ic_launcher)
                .backgroundColor(Color.WHITE)
                .title(R.string.redeemtion_failed_title)
                .titleColor(Color.BLACK)
                .content(info)
                .contentGravity(GravityEnum.CENTER)
                .contentColor(Color.GRAY)
                .positiveText(R.string.redeemtion_failed_tutup)
                .positiveColor(Color.GRAY)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .cancelable(true)
                .show();
    }

    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.tvTryAgain)
    void retryConnection() {
        presenter.packageDetail(investment);
    }


    @OnClick(R.id.bDetail)
    void bDetail(){
        if(pageNumber > 0){
            pageNumber--;
            pager.setCurrentItem(pageNumber);

            if(status == 0){
                bDetail.setBackgroundResource(R.drawable.left_bg_round_white);
                bPerformaInvest.setBackgroundResource(R.drawable.right_bg_round_blue);
                bDetail.setTextColor(getResources().getColor(R.color.colorPrimary));
                bPerformaInvest.setTextColor(getResources().getColor(R.color.white));
                status = 1;
            }else if(status == 1){
                bDetail.setBackgroundResource(R.drawable.left_bg_round_blue);
                bPerformaInvest.setBackgroundResource(R.drawable.right_bg_round_white);
                bDetail.setTextColor(getResources().getColor(R.color.white));
                bPerformaInvest.setTextColor(getResources().getColor(R.color.colorPrimary));
                status = 0;
            }
        }
    }


    @OnClick(R.id.bPerformaInvest)
    void bPerformaInvest(){
        if (pageNumber < detailPortfolioAdapter.getCount() - 1){
            pageNumber++;
            pager.setCurrentItem(pageNumber);

            if(status == 0){
                bDetail.setBackgroundResource(R.drawable.left_bg_round_white);
                bPerformaInvest.setBackgroundResource(R.drawable.right_bg_round_blue);
                bDetail.setTextColor(getResources().getColor(R.color.colorPrimary));
                bPerformaInvest.setTextColor(getResources().getColor(R.color.white));
                status = 1;
            }else if(status == 1){
                bDetail.setBackgroundResource(R.drawable.left_bg_round_blue);
                bPerformaInvest.setBackgroundResource(R.drawable.right_bg_round_white);
                bDetail.setTextColor(getResources().getColor(R.color.white));
                bPerformaInvest.setTextColor(getResources().getColor(R.color.colorPrimary));
                status = 0;
            }
        }

    }


    public void validationProductReguler(){
        if(accountType.equals(Constant.INVEST_TYPE_REGULER)){
            lnTopUp.setVisibility(View.GONE);
        }
    }

}
