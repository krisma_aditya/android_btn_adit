package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.Bind;

/**
 * Created by asep.surahman on 15/05/2018.
 */

public class Kyc5EmploymentDataOneFragment extends KycBaseNew {

    public static final String TAG = Kyc5EmploymentDataOneFragment.class.getSimpleName();

    @Bind(R.id.sOccoupation)
    Spinner sOccoupation;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etCompanyName)
    EditText etCompanyName;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etJobPosition)
    EditText etJobPosition;


    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc5EmploymentDataOneFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    public static Fragment getFragment(KycDataRequest kycDataRequest){
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        Kyc5EmploymentDataOneFragment fragment = new Kyc5EmploymentDataOneFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected int getLayout() {
        return R.layout.f_register_data_pekerjaan_1;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }


    @Override
    public void onResume(){
        super.onResume();
    }

    public void init(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            disableField();
        }

        etCompanyName.setText(request.getEmployer());
        etJobPosition.setText(request.getJobPosition());
        setupSpinnerWithSpecificLookupSelection(sOccoupation, getKycLookupFromRealm(Constant.KYC_CAT_OCCUPATION), request.getOccupation());
    }



    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }else{
            if(!validate()){
                onValidFailed();
            }else{
                setValueEmploymentData();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }
            return;
        }
    }

    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    @Override
    public void saveDataKycWithBackpress(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
                setValueEmploymentData();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }

    @Override
    public void previewsWithoutValidation() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }


    //disable
    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();

    }


    public void setValueEmploymentData(){
        request.setJobPosition(getAndTrimValueFromEditText(etJobPosition));
        request.setEmployer(getAndTrimValueFromEditText(etCompanyName));
        request.setOccupation(getKycLookupCodeFromSelectedItemSpinner(sOccoupation));
    }


    public boolean validate(){
        boolean valid = true;
        String jobPosition  = etJobPosition.getText().toString();
        String companyName  = etCompanyName.getText().toString();
        String ocuppation   = getKycLookupCodeFromSelectedItemSpinner(sOccoupation);

        if(sOccoupation.getSelectedItem().toString().equals("") ||
            sOccoupation.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            valid = false;
            ((TextView) sOccoupation.getSelectedView()).setError("Mohon isi kolom ini");
        }

        if(!ocuppation.equals("")){
            if(!ocuppation.equals("IRT")){

                if(jobPosition.isEmpty() && jobPosition.length() <= 0){
                    etJobPosition.setError("Mohon isi kolom ini");
                    valid = false;
                }

                if(!jobPosition.isEmpty() && jobPosition.length() < 3){
                    etJobPosition.setError("Mohon isi jabatan dengan benar");
                    valid = false;
                }

                if(companyName.isEmpty() && companyName.length() <= 0){
                    etCompanyName.setError("Mohon isi kolom ini");
                    valid = false;
                }

                if(!companyName.isEmpty() && companyName.length() < 3){
                    etCompanyName.setError("Mohon isi nama perusahaan dengan benar");
                    valid = false;
                }
            }
        }


        return valid;
    }


    public void disableField(){
        etJobPosition.setEnabled(false);
        etCompanyName.setEnabled(false);
        sOccoupation.setEnabled(false);
    }


    @Override
    public void previewsWhileError(){

    }

}
