package com.danareksa.investasik.ui.fragments.promo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.PromoResponse;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;

public class SyaratKetentuanPromoFragment extends BaseFragment {

    public static final String TAG = SyaratKetentuanPromoFragment.class.getSimpleName();

    @Override
    protected int getLayout() {
        return R.layout.f_syarat_ketentuan_promo;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public static void showFragment(BaseActivity sourceActivity, PromoResponse response) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new SyaratKetentuanPromoFragment(),TAG);
            fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, R.anim.slide_out_left, android.R.anim.slide_in_left, R.anim.slide_out_left);
            fragmentTransaction.commit();
        }
    }
}