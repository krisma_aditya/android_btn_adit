package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.City;
import com.danareksa.investasik.data.api.beans.Country;
import com.danareksa.investasik.data.api.beans.KycLookup;
import com.danareksa.investasik.data.api.beans.LookupAddress;
import com.danareksa.investasik.data.api.beans.State;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.adapters.rv.CorrespondenceAdapter;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.danareksa.investasik.util.ui.RecyclerItemClickListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;


/**
 * Created by asep.surahman on 21/05/2018.
 */

public class Kyc9AdvancedCorrespondenceFragment extends KycBaseNew {

    public static final String TAG = Kyc9AdvancedCorrespondenceFragment.class.getSimpleName();

    public static final String KYC_DATA_REQUEST = "kycDataRequest";

    protected Validator validator;
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;

    @Bind(R.id.sCountry)
    Spinner sCountry;
    @Bind(R.id.sState)
    Spinner sState;
    @Bind(R.id.sCity)
    Spinner sCity;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etOtherAddress)
    EditText etOtherAddress;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etKodePos)
    EditText etKodePos;

    @Bind(R.id.llOtherAddress)
    LinearLayout llOtherAddress;

    @Bind(R.id.tOtherAddress)
    TextView tOtherAddress;

    @Bind(R.id.llDetailAddress)
    LinearLayout llDetailAddress;

    @Bind(R.id.rv)
    RecyclerView rv;

    public List<Country> countries;
    public List<State> states;
    public List<City> cities;
    public KycDataRequest kycDataRequest;
    private String jsonCountry;
    private String jsonState;
    private String jsonCity;
    String mailingAddressType = "";

    String countryId = "";
    String stateId = "";

    Kyc9AdvancedCorrespondencePrensenter presenter;
    List<LookupAddress> kycLookups;

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc9AdvancedCorrespondenceFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    public static Fragment getFragment(KycDataRequest kycDataRequest, String jsonCountry, String jsonState, String jsonCity) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        bundle.putString("jsonCountry", jsonCountry);
        bundle.putString("jsonState", jsonState);
        bundle.putString("jsonCity", jsonCity);
        Kyc9AdvancedCorrespondenceFragment kyc9AdvancedCorrespondenceFragment = new Kyc9AdvancedCorrespondenceFragment();
        kyc9AdvancedCorrespondenceFragment.setArguments(bundle);
        return kyc9AdvancedCorrespondenceFragment;
    }


    @Override
    protected int getLayout(){
        return R.layout.f_register_lanjut_surat_menyurat;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        kycDataRequest = (KycDataRequest) getArguments().getSerializable(KYC_DATA_REQUEST);
        jsonCountry = getArguments().getString("jsonCountry");
        jsonState = getArguments().getString("jsonState");
        jsonCity = getArguments().getString("jsonCity");
        presenter = new Kyc9AdvancedCorrespondencePrensenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        llDetailAddress.setVisibility(View.GONE);

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 1 : 1;
            }
        });

        rv.setLayoutManager(layoutManager);
        setLookupAddress();
        rv.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.SimpleOnItemClickListener(){
            @Override
            public void onItemClick(View childView, int position){
                super.onItemClick(childView, position);
                LookupAddress kycLookup = (LookupAddress) childView.getTag();

                if(kycLookup.getCode().toString().equals("3")){

                    if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")){

                        checkAddressActive(kycLookup);
                        mailingAddressType = kycLookup.getCode().toString();
                        llDetailAddress.setVisibility(View.VISIBLE);
                        etOtherAddress.setText(request.getMailingAddress());
                        etKodePos.setText(request.getMailingPostalCode());

                        Type listType = new TypeToken<List<Country>>(){}.getType();
                        Gson gson = new Gson();
                        countries = gson.fromJson(jsonCountry, listType);

                        Type listTypeState = new TypeToken<List<State>>(){}.getType();
                        Gson gsonState = new Gson();
                        states = gsonState.fromJson(jsonState, listTypeState);

                        Type listTypeCity = new TypeToken<List<City>>(){}.getType();
                        Gson gsonCity = new Gson();
                        cities = gsonCity.fromJson(jsonCity, listTypeCity);

                        if(request.getMailingCountry() != null) {
                            setupSpinnerCountry(sCountry, countries, request.getMailingCountry());
                        }else{
                            setupSpinnerCountry(sCountry, countries, Constant.ID_COUNTRY_INDO);
                        }

                        setupSpinnerState(sState, states, request.getMailingProvince());
                        setupSpinnerCity(sCity, cities, request.getMailingCity());

                        if (request.getMailingCountry() != null) {
                            if (request.getMailingCountry().equals(Constant.ID_COUNTRY_INDO)) {

                                stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());
                                if (!stateId.equals("")) {
                                    presenter.getCity(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
                                } else {
                                    presenter.getCity(String.valueOf(3));
                                }

                            } else {
                                if (!request.getMailingCountry().equals("")) {
                                    presenter.getState(request.getMailingCountry(), request.getMailingProvince());
                                }
                            }
                        } else {

                            stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());
                            if (!stateId.equals("")) {
                                presenter.getCity(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
                            } else {
                                presenter.getCity(String.valueOf(3));
                            }
                        }

                        onItemSelectedItem();
                    }
                }else{
                    if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {

                        checkAddressActive(kycLookup);
                        if(request.getOccupation().equals("IRT") && kycLookup.getCode().equals("2")){
                        }else{
                            chooseCodeSelain3(kycLookup.getCode());
                        }
                    }
                }
            }
        }));

        init();
    }


    private void checkAddressActive(LookupAddress kycLookup){
        if(kycLookups != null && kycLookups.size() > 0){
            for(int i = 0; i < kycLookups.size(); i++){
                if(kycLookups.get(i).getCode().equals(kycLookup.getCode())){

                    if(request.getOccupation() != null){
                        if(request.getOccupation().equals("IRT") && kycLookup.getCode().equals("2")){
                            kycLookups.get(i).setStatus(false);
                        }else{
                            kycLookups.get(i).setStatus(true);
                            updateStatus(kycLookup.getCode());
                        }
                    }else{
                        kycLookups.get(i).setStatus(true);
                        updateStatus(kycLookup.getCode());
                    }

                    break;
                }
            }
            loadList();
        }
    }

    private void updateStatus(String code){
        for(int i = 0; i < kycLookups.size(); i++){
            if(!kycLookups.get(i).getCode().equals(code)){
                kycLookups.get(i).setStatus(false);
            }
        }
    }


    private void setLookupAddress(){
        kycLookups = new ArrayList<>();
        if(getKycLookupFromRealm(Constant.KYC_CAT_STATEMENT_TYPE) != null && getKycLookupFromRealm(Constant.KYC_CAT_STATEMENT_TYPE).size() > 0){
            for(KycLookup kycLookup : getKycLookupFromRealm(Constant.KYC_CAT_STATEMENT_TYPE)){
                if(!kycLookup.equals("") && !kycLookup.getCode().equals("")){
                    LookupAddress lookupAddress = new LookupAddress();
                    lookupAddress.setCategory(kycLookup.getCategory());
                    lookupAddress.setCode(kycLookup.getCode());
                    lookupAddress.setSeq(kycLookup.getSeq());
                    lookupAddress.setValue(kycLookup.getValue());
                    lookupAddress.setStatus(false);
                    kycLookups.add(lookupAddress);
                }
            }
        }
    }


    public void loadList(){
        rv.setAdapter(new CorrespondenceAdapter(getActivity(), kycLookups));
    }

    @Override
    public void onResume(){
        super.onResume();
    }


    public void init(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            disableField();
        }

        if(request.getMailingAddressType() != null){
            for(int i = 0; i < kycLookups.size(); i++){
                if(kycLookups.get(i).getCode().equals(request.getMailingAddressType())){

                    //jika kode == 3
                    if(request.getMailingAddressType().equals("3")){

                        kycLookups.get(i).setStatus(true);
                        mailingAddressType = kycLookups.get(i).getCode();
                        llDetailAddress.setVisibility(View.VISIBLE);
                        etOtherAddress.setText(request.getMailingAddress());
                        etKodePos.setText(request.getMailingPostalCode());

                        Type listType = new TypeToken<List<Country>>(){}.getType();
                        Gson gson = new Gson();
                        countries = gson.fromJson(jsonCountry, listType);

                        Type listTypeState = new TypeToken<List<State>>() {}.getType();
                        Gson gsonState = new Gson();
                        states = gsonState.fromJson(jsonState, listTypeState);

                        Type listTypeCity = new TypeToken<List<City>>() {}.getType();
                        Gson gsonCity = new Gson();
                        cities = gsonCity.fromJson(jsonCity, listTypeCity);

                        if(request.getMailingCountry() != null){
                            setupSpinnerCountry(sCountry, countries, request.getMailingCountry());
                        }else{
                            setupSpinnerCountry(sCountry, countries, Constant.ID_COUNTRY_INDO);
                        }

                        setupSpinnerState(sState, states, request.getMailingProvince());
                        setupSpinnerCity(sCity, cities, request.getMailingCity());

                        if(request.getMailingCountry() != null){
                            if(request.getMailingCountry().equals(Constant.ID_COUNTRY_INDO)){

                                stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());
                                if(!stateId.equals("")){
                                    presenter.getCity(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
                                }else{
                                    presenter.getCity(String.valueOf(3));
                                }

                            }else{
                                if(!request.getMailingCountry().equals("")){
                                    presenter.getState(request.getMailingCountry(), request.getMailingProvince());
                                }
                            }
                        }else{

                            stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());
                            if(!stateId.equals("")){
                                presenter.getCity(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
                            }else{
                                presenter.getCity(String.valueOf(3));
                            }
                        }

                        onItemSelectedItem();

                    }else{ //kode selain 3


                        if(request.getOccupation() != null){
                            if(request.getOccupation().equals("IRT")  && kycLookups.get(i).getCode().equals("2")){
                                kycLookups.get(i).setStatus(false);
                            }else{
                                kycLookups.get(i).setStatus(true);
                            }
                        }else{
                            kycLookups.get(i).setStatus(true);
                        }

                        mailingAddressType = kycLookups.get(i).getCode();
                        llDetailAddress.setVisibility(View.GONE);
                    }

                }
            }
        }


        loadList();

    }


    private void chooseCodeSelain3(String code){
        mailingAddressType = code;
        llDetailAddress.setVisibility(View.GONE);
    }


    public void onItemSelectedItem() {
        sCountry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                countryId = String.valueOf(countries.get(sCountry.getSelectedItemPosition()).getId());

                if(sCountry.getSelectedItem().toString().equals("")){
                    countryId = "";
                    states = new ArrayList<>();
                    setupSpinnerState(sState, states, null);

                    cities = new ArrayList<>();
                    setupSpinnerCity(sCity, cities, null);
                }else{
                    if(!countryId.equals("")){
                        presenter.getState(countryId, "");
                    }
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        sState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                stateId = String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode());

                if (!stateId.equals("")) {
                    presenter.getCity(stateId);
                }else{
                    stateId = "";
                    cities = new ArrayList<>();
                    setupSpinnerCity(sCity, cities, null);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView){

            }
        });
    }


    private void setValueAdvanceCorrespondence(){
        if(mailingAddressType.equals(Constant.mailingAddressTypeKtp)){
            request.setMailingAddressType(Constant.mailingAddressTypeKtp);
            request.setMailingPostalCode(request.getIdPostalCode());
            request.setMailingAddress(request.getIdAddress());
            request.setMailingCountry(request.getIdCountry());
            request.setMailingProvince(request.getIdProvince());
            request.setMailingCity(request.getIdCity());
        }else if(mailingAddressType.equals(Constant.mailingAddressTypeOffice)){

            if(request.getOccupation() != null){
                if(!request.getOccupation().equals("IRT")){
                    request.setMailingAddressType(Constant.mailingAddressTypeOffice);
                    request.setMailingAddress(request.getOfficeAddress());
                    request.setMailingCountry(request.getOfficeCountry());
                    request.setMailingProvince(request.getOfficeProvince());
                    request.setMailingCity(request.getOfficeCity());
                    request.setMailingPostalCode(request.getOfficePostalCode());
                }
            }

        }else{
            request.setMailingAddressType(mailingAddressType);
            request.setMailingPostalCode(getAndTrimValueFromEditText(etKodePos));
            request.setMailingAddress(getAndTrimValueFromEditText(etOtherAddress));
            request.setMailingCountry(String.valueOf(countries.get(sCountry.getSelectedItemPosition()).getId()));
            request.setMailingProvince(String.valueOf(states.get(sState.getSelectedItemPosition()).getStateCode()));
            request.setMailingCity(String.valueOf(cities.get(sCity.getSelectedItemPosition()).getId()));
        }
    }


    @Override
    public void saveDataKycWithBackpress(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if (PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
                setValueAdvanceCorrespondence();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            } else {
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }

    @Override
    public void previewsWithoutValidation() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }




    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {

            if(request.getFundSource() != null){
                if(request.getFundSource().equals("FAM") || request.getFundSource().equals("SPS") || request.getFundSource().equals("OTH")){ //jika sumber dana berasal orangtua / anak, suami/istri, lainnya harus mengisi sumber dana tambahan
                    getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
                }else{
                    getBus().send(new RxBusObject(RxBusObject.RxBusKey.TO_PAGE_BENEFECERY, null));
                }
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }


        }else{
            if(!validate()){
                onValidFailed();
            }else{
                if(request.getFundSource().equals("FAM") || request.getFundSource().equals("SPS") || request.getFundSource().equals("OTH")){ //jika sumber dana berasal orangtua / anak, suami/istri, lainnya harus mengisi sumber dana tambahan
                    setValueAdvanceCorrespondence();
                    getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
                }else{
                    setValueAdvanceCorrespondence();
                    getBus().send(new RxBusObject(RxBusObject.RxBusKey.TO_PAGE_BENEFECERY, null));
                }
            }
            return;
        }

    }

    public void onValidFailed() {
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    //disable
    @Override
    public void onValidationSucceeded() {
        super.onValidationSucceeded();
    }


    public boolean validate(){
        boolean valid = true;
        String otherAddress = etOtherAddress.getText().toString();
        String postalKode = etKodePos.getText().toString();

        if(mailingAddressType.equals("")){
            Toast.makeText(getContext(), "Pilih tipe alamat", Toast.LENGTH_LONG).show();
            valid = false;
        }

        if(mailingAddressType.equals(Constant.mailingAddressTypeLain)){

            //https://support.jatis.com/browse/DIMRDO-1293
//            if(postalKode.isEmpty() && postalKode.length() <= 0){
//                valid = false;
//                etKodePos.setError("Mohon isi kolom ini");
//            }

            if(otherAddress.isEmpty() && otherAddress.length() <= 0){
                valid = false;
                etOtherAddress.setError("Mohon isi kolom ini");
            }

            if(!otherAddress.isEmpty() && otherAddress.length() < 3){
                valid = false;
                etOtherAddress.setError("Mohon isi alamat dengan benar");
            }

            if(sCountry.getSelectedItem().toString().equals("") ||
                sCountry.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
                valid = false;
                ((TextView) sCountry.getSelectedView()).setError("Mohon isi kolom ini");
            }
            if(sState.getSelectedItem().toString().equals("") ||
                sState.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
                valid = false;
                ((TextView) sState.getSelectedView()).setError("Mohon isi kolom ini");
            }
            if(sCity.getSelectedItem().toString().equals("") ||
                sCity.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
                valid = false;
                ((TextView) sCity.getSelectedView()).setError("Mohon isi kolom ini");
            }
        }

        return valid;
    }


    public void showProgressBar() {
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar() {
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }


    public void disableField() {
        sCountry.setEnabled(false);
        sState.setEnabled(false);
        sCity.setEnabled(false);
        etOtherAddress.setEnabled(false);
        etKodePos.setEnabled(false);
    }

    @Override
    public void previewsWhileError(){

    }


}
