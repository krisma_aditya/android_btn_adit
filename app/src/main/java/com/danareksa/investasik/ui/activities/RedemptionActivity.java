package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.Packages;
import com.danareksa.investasik.data.api.beans.PortfolioInvestment;
import com.danareksa.investasik.ui.fragments.redemption.ConfirmationNewFragment;

import butterknife.Bind;
import icepick.State;

/**
 * Created by fajarfatur on 3/14/16.
 */
public class RedemptionActivity extends BaseActivity {

    private static final String INVESTMENT = "investment";
    private static final String PACKAGES = "packages";
    private static final String IFUA = "ifua";
    private static final String STATUS_SPECIAL_FEE = "statusSpecialFee";
    private static final String SPECIAL_FEE = "specialFee";
    private static final String TOTAL_DAYS = "totalDays";



    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.v1)
    View v1;
    @Bind(R.id.v2)
    View v2;
    @Bind(R.id.v3)
    View v3;
    @Bind(R.id.v4)
    View v4;
    @Bind(R.id.step1)
    ImageView step1;
    @Bind(R.id.step2)
    ImageView step2;
    @Bind(R.id.step3)
    ImageView step3;

    @State
    boolean statusSpecialFee;
    @State
    double specialFee;
    @State
    Integer totalDays;
    @State
    String ifua;
    @State
    PortfolioInvestment investment;
    @State
    Packages packages;
    @State
    public int step = 1;

    public static void startActivity(BaseActivity sourceActivity, PortfolioInvestment investment, Packages packages,
                                     String ifua, boolean statusSpecialFee, double specialFee, Integer totalDays){
        Intent intent = new Intent(sourceActivity, RedemptionActivity.class);
        intent.putExtra(INVESTMENT, investment);
        intent.putExtra(PACKAGES, packages);
        intent.putExtra(IFUA, ifua);
        intent.putExtra(STATUS_SPECIAL_FEE, statusSpecialFee);
        intent.putExtra(SPECIAL_FEE, specialFee);
        intent.putExtra(TOTAL_DAYS, totalDays);
        sourceActivity.startActivity(intent);
    }

    @Override
    protected int getLayout() {
        return R.layout.a_redemption;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(null);
        }

        if(investment == null)
            investment = (PortfolioInvestment) getIntent().getSerializableExtra(INVESTMENT);

        if(packages == null)
            packages = (Packages) getIntent().getSerializableExtra(PACKAGES);


        Bundle b = getIntent().getExtras();
        double result = b.getDouble("key");

        ifua = getIntent().getStringExtra(IFUA);

        specialFee = (Double) getIntent().getSerializableExtra(SPECIAL_FEE);
        statusSpecialFee = (Boolean) getIntent().getSerializableExtra(STATUS_SPECIAL_FEE);
        totalDays = (Integer) getIntent().getSerializableExtra(TOTAL_DAYS);

        title.setText("Penjualan Kembali");
        ConfirmationNewFragment.showFragment(this, investment, packages, ifua, statusSpecialFee, specialFee, totalDays);
        setupStep();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        finish();
    }

    public void setStep(int step) {
        this.step = step;
        setupStep();
    }

    void setupStep() {
        switch (step) {
            case 1:
                v1.setVisibility(View.INVISIBLE);
                v2.setVisibility(View.INVISIBLE);
                v3.setVisibility(View.INVISIBLE);
                v4.setVisibility(View.INVISIBLE);

                step2.setImageResource(R.drawable.bg_primary_oval_stroke);
                step3.setImageResource(R.drawable.bg_primary_oval_stroke);
                break;

            case 2:
                v1.setVisibility(View.VISIBLE);
                v2.setVisibility(View.VISIBLE);
                v3.setVisibility(View.INVISIBLE);
                v4.setVisibility(View.INVISIBLE);

                step2.setImageResource(R.drawable.bg_primary_oval);
                step3.setImageResource(R.drawable.bg_primary_oval_stroke);
                break;

            case 3:
                v1.setVisibility(View.VISIBLE);
                v2.setVisibility(View.VISIBLE);
                v3.setVisibility(View.VISIBLE);
                v4.setVisibility(View.VISIBLE);

                step2.setImageResource(R.drawable.bg_primary_oval);
                step3.setImageResource(R.drawable.bg_primary_oval);
                break;
        }
    }
}
