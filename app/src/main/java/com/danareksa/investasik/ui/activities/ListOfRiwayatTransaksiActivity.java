package com.danareksa.investasik.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.fragments.riwayattransaksi.ListOfRiwayatTransaksiFragment;

import java.util.HashMap;

import butterknife.Bind;

/**
 * Created by asep.surahman on 29/06/2018.
 */

public class ListOfRiwayatTransaksiActivity extends BaseActivity {

    static final String PARAMS = "PARAMS";

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.title)
    TextView title;

    public static void startActivity(BaseActivity sourceActivity, HashMap<String, String> params) {
        Intent intent = new Intent(sourceActivity, ListOfRiwayatTransaksiActivity.class);
        intent.putExtra(PARAMS, params);
        sourceActivity.startActivity(intent);
    }


    @Override
    protected int getLayout() {
        return R.layout.a_riwayat_transaksi;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSupportActionBar(toolbar);
        setTitle("");

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        title.setText("Riwayat Transaksi");

        Intent intent = getIntent();
        HashMap<String, String> hashMap = (HashMap<String, String>)intent.getSerializableExtra(PARAMS);

        ListOfRiwayatTransaksiFragment.showFragment(this, hashMap);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        //startActivity(new Intent(ListOfRiwayatTransaksiActivity.this, RiwayatTransaksiActivity.class));
        finish();
    }



}
