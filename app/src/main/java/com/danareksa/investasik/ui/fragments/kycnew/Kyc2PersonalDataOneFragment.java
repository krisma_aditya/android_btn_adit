package com.danareksa.investasik.ui.fragments.kycnew;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.DateUtil;
import com.danareksa.investasik.util.eventBus.RxBusObject;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 15/05/2018.
 */

public class Kyc2PersonalDataOneFragment extends KycBaseNew{

    public static final String TAG = Kyc2PersonalDataOneFragment.class.getSimpleName();
    protected Validator validator;

    @Bind(R.id.ivMale)
    ImageView ivmale;

    @Bind(R.id.ivFemale)
    ImageView ivFemale;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etBirthPlace)
    EditText etBirthPlace;

    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etBirthdate)
    EditText etBirthdate;

    @Bind(R.id.sMaritalStatus)
    Spinner sMaritalStatus;

    @Bind(R.id.tvGender)
    TextView tvGender;

    @Bind(R.id.sReligion)
    Spinner sReligion;

    String gender = "";

    String birthDate;
    public KycDataRequest kycDataRequest;

    public static void showFragment(BaseActivity sourceActivity){
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc2PersonalDataOneFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    public static Fragment getFragment(KycDataRequest kycDataRequest){
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        Kyc2PersonalDataOneFragment kyc2PersonalDataOneFragment = new Kyc2PersonalDataOneFragment();
        kyc2PersonalDataOneFragment.setArguments(bundle);
        return kyc2PersonalDataOneFragment;
    }


    @Override
    protected int getLayout(){
        return R.layout.f_register_data_pribadi_1;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }


    @Override
    public void onResume(){
        super.onResume();
    }


    @OnClick(R.id.etBirthdate)
    void etBirthdate(){
        DateUtil.showDatePickerDialog(getActivity().getSupportFragmentManager(), new DateUtil.DateDialogPickerListener() {
            @Override
            public void onDatePick(String date, String month, String year){
                etBirthdate.setText(date + " " + DateUtil.getMonthString(month) + " " + year);
                birthDate = year + "-" + DateUtil.getMonthNumber(month) + "-" + DateUtil.getDateNumberString(date);
                etBirthdate.setError(null);

            }
        }, DateUtil.DatePickerFragment.TYPE_BIRTH_DATE);
    }


    public void init(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            disableField();
            tvGender.setText("Jenis kelamin Anda");
        }

        if(request.getBirthDate() != null){
            birthDate = request.getBirthDate();
            if(!birthDate.equals("")){
                String str[] = birthDate.split("-");
                String year  = String.valueOf(str[0]);
                String month = String.valueOf(str[1]);
                String date  = String.valueOf(str[2]);
                etBirthdate.setText(date + " " + DateUtil.getMonthString(String.valueOf(Integer.parseInt(month)-1)) + " " + year);
            }else{
                etBirthdate.setText(request.getBirthDate());
            }
        }

        if(request.getGender() != null){
            if(request.getGender().equals(Constant.GENDER_MALE)){
                gender = Constant.GENDER_MALE;
                getActiveMale();
            }else if(request.getGender().equals(Constant.GENDER_FEMALE)){
                gender = Constant.GENDER_FEMALE;
                getActiveFemale();
            }
        }else{
            ivmale.setImageResource(R.drawable.ic_men_aktif);
            ivFemale.setImageResource(R.drawable.ic_woman_aktif);
        }

        etBirthPlace.setText(request.getBirthPlace());
        setupSpinnerWithSpecificLookupSelection(sMaritalStatus, getKycLookupFromRealm(Constant.KYC_CAT_MARITAL_STATUS), request.getMaritalStatus());
        setupSpinnerWithSpecificLookupSelection(sReligion, getKycLookupFromRealm(Constant.KYC_CAT_RELIGION), request.getReligion());
    }



    public void setValuePersonalData(){
        request.setGender(gender);
        System.out.println("birth date : " + birthDate);
        request.setBirthDate(birthDate); //sudah diinsert pas register awal
        request.setBirthPlace(getAndTrimValueFromEditText(etBirthPlace));
        request.setMaritalStatus(getKycLookupCodeFromSelectedItemSpinner(sMaritalStatus));
        request.setReligion(getKycLookupCodeFromSelectedItemSpinner(sReligion));
    }



    @Override
    public void saveDataKycWithBackpress(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
                setValuePersonalData();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }


    @Override
    public void previewsWithoutValidation() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }



    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }else{
            if(!validate()){
                onValidFailed();
            }else{
                setValuePersonalData();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }
            return;
        }
    }


    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    public boolean validate(){

        boolean valid = true;
        String birthPlace = etBirthPlace.getText().toString();
        String birthDate  = etBirthdate.getText().toString();

        if(gender.equals("")){
            valid = false;
            etBirthPlace.setError("Mohon isi jenis kelamin");
        }

        if(birthPlace.isEmpty() && birthPlace.length() <= 0){
            etBirthPlace.setError("Mohon isi kolom ini");
            valid = false;
        }else{
            etBirthPlace.setError(null);
        }

        if(birthDate.isEmpty() && birthDate.length() <= 0){
            etBirthdate.setError("Mohon isi kolom ini");
            valid = false;
        }else{
            etBirthdate.setError(null);
        }

        if(sMaritalStatus.getSelectedItem().toString().equals("") ||
                sMaritalStatus.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            ((TextView) sMaritalStatus.getSelectedView()).setError("Mohon isi kolom ini");
            valid = false;
        }

        if(sReligion.getSelectedItem().toString().equals("") ||
                sReligion.getSelectedItem().toString().equalsIgnoreCase("Silahkan Pilih")){
            ((TextView) sReligion.getSelectedView()).setError("Mohon isi kolom ini");
            valid = false;
        }

        return valid;
    }


    //disable
    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
    }


    public void disableField(){
        etBirthPlace.setEnabled(false);
        etBirthdate.setEnabled(false);
        sMaritalStatus.setEnabled(false);
        sReligion.setEnabled(false);
    }


    @OnClick(R.id.ivMale)
    void ivMale(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            gender = Constant.GENDER_MALE;
            getActiveMale();
        }
    }


    @OnClick(R.id.ivFemale)
    void ivFemale(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            gender = Constant.GENDER_FEMALE;
            getActiveFemale();
        }
    }


    private void getActiveMale(){
        ivFemale.setImageResource(R.drawable.ic_woman_aktif);
        ivmale.setImageResource(R.drawable.ic_checked_circle);
    }

    private void getActiveFemale(){
        ivFemale.setImageResource(R.drawable.ic_checked_circle);
        ivmale.setImageResource(R.drawable.ic_men_aktif);
    }



















    /*@OnClick({ R.id.radioMale, R.id.radioFemale})
    public void onRadioButtonClicked(RadioButton radioButton){
        boolean checked = radioButton.isChecked();
        switch (radioButton.getId()){
            case R.id.radioMale:
                if(checked){
                    gender = "ML";
                }
                break;
            case R.id.radioFemale:
                if(checked){
                    gender = "FML";
                }
                break;
        }
    }*/

    @Override
    public void previewsWhileError(){

    }


}
