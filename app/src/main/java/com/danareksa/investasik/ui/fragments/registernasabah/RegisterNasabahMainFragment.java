package com.danareksa.investasik.ui.fragments.registernasabah;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danareksa.investasik.R;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.adapters.pager.RegisterNasabahPageAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.ui.NonSwipeableViewPager;
import com.danareksa.investasik.util.ui.WrapContentHeightViewPager;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by asep.surahman on 03/07/2018.
 */

public class RegisterNasabahMainFragment extends BaseFragment {


    @Bind(R.id.pager)
    NonSwipeableViewPager pager;

    @Bind(R.id.bBaru)
    TextView bBaru;

    @Bind(R.id.bLama)
    TextView bLama;

    @Bind(R.id.bNasBaru)
    TextView bNasBaru;

    @Bind(R.id.bNasLama)
    TextView bNasLama;

    private RegisterNasabahPageAdapter pagerAdapter;

    @State
    int pageNumber = 0;

    public static final String TAG = RegisterNasabahMainFragment.class.getSimpleName();

    public static void showFragment(BaseActivity sourceActivity) {
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new RegisterNasabahMainFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayout(){
        return R.layout.f_register_nasabah_fix;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        setupViewPager();
        pager.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @OnClick(R.id.llNewCustomer)
    void llNewCustomer(){
        pager.setVisibility(View.VISIBLE);
        bBaru.setText(getCheckedIcon());
        if(pageNumber == 1){
            pageNumber = 0;
        }
        pager.setCurrentItem(pageNumber);
    }

    @OnClick(R.id.llOldCustomer)
    void llOldCustomer(){
        pager.setVisibility(View.VISIBLE);
        bLama.setText(getCheckedIcon());
        if (pageNumber == 0) {
            pageNumber = 1;
        }
        pager.setCurrentItem(pageNumber);
    }


    public void setupViewPager(){
        pagerAdapter = new RegisterNasabahPageAdapter(this, getChildFragmentManager());
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(new WrapContentHeightViewPager.OnPageChangeListener(){

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels){

            }

            @Override
            public void onPageSelected(int position){
                switch (position){
                    case 0:
                        bLama.setText("B");
                        bBaru.setText(getCheckedIcon());
                        break;
                    case 1:
                        bBaru.setText("A");
                        bLama.setText(getCheckedIcon());
                        break;
                    default:
                }
            }

            @Override
            public void onPageScrollStateChanged(int state){

            }
        });
    }

    private Spannable getCheckedIcon(){
        Spannable buttonLabel = new SpannableString(" ");
        buttonLabel.setSpan(new ImageSpan(getActivity().getApplicationContext(), R.drawable.ic_checked,
                ImageSpan.ALIGN_BOTTOM), 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return  buttonLabel;
    }

    public boolean isPageVisible(){
        return pager.getVisibility() == View.VISIBLE;
    }

    public void handleBackPressed() {
        bBaru.setText("A");
        bLama.setText("B");
        pageNumber = 0;
        pager.setVisibility(View.INVISIBLE);
    }


}
