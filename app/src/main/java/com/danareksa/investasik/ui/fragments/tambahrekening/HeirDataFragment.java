package com.danareksa.investasik.ui.fragments.tambahrekening;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.HeirList;
import com.danareksa.investasik.data.api.beans.HeirListReguler;
import com.danareksa.investasik.data.api.beans.KycLookup;
import com.danareksa.investasik.data.api.requests.AddAccountRequest;
import com.danareksa.investasik.ui.activities.BeneficiaryStatementActivity;
import com.danareksa.investasik.ui.adapters.rv.HeirDataAdapter;
import com.danareksa.investasik.util.Constant;
import com.danareksa.investasik.util.eventBus.RxBusObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;


/**
 * Created by asep.surahman on 24/05/2018.
 */

public class HeirDataFragment extends BaseInvest implements HeirDataAdapter.CallbackRemoveHeir {

    public static final String TAG = HeirDataFragment.class.getSimpleName();
    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;
    @Bind(R.id.rv)
    RecyclerView rv;
    @Bind(R.id.btnAdd)
    Button btnAdd;
    @Bind(R.id.btnNext)
    Button btnNext;
    @Bind(R.id.cbAccept)
    CheckBox cbAccept;
    public List<HeirListReguler> heirListRegulers;
    String typeAccount;
    HeirDataAdapter adapter;
    List<KycLookup> kycLookupRelationship;
    public static final String ADD_ACCOUNT_REQUEST = "accountRequest";


    public static Fragment getFragment(String typeAccount, AddAccountRequest acountRequest){
        Bundle bundle = new Bundle();
        bundle.putString("type", typeAccount);
        bundle.putSerializable(ADD_ACCOUNT_REQUEST, acountRequest);
        HeirDataFragment fragment = new HeirDataFragment();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected int getLayout() {
        return R.layout.f_tambah_rekening_data_ahli_waris;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        typeAccount = getArguments().getString("type");
        heirListRegulers = new ArrayList<>();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        cbAccept.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)  showTerms();
            }
        });
    }


    private void showTerms() {
        Intent intent = new Intent(this.getActivity(), BeneficiaryStatementActivity.class);
        intent.putExtra("type",typeAccount);
        this.getActivity().startActivity(intent);
    }

    private void init(){
        kycLookupRelationship = new ArrayList<>();
        kycLookupRelationship = getKycLookupFromRealm(Constant.KYC_CAT_RELATIONSHIP_HEIR);
        loadFirstRole();
    }

    @Override
    public void onResume(){
        super.onResume();
    }


    @OnClick(R.id.btnAdd)
    void btnAdd(){
        loadRole();
    }


    public void loadRole(){
        if(heirListRegulers != null && heirListRegulers.size() != 0){
            updateListPackage();
            addDataHeir();
        }else{
            addFirstDataHeir();
        }
        loadList();
    }


    public void loadFirstRole(){
        if(heirListRegulers != null && heirListRegulers.size() != 0){
            addFirstDataHeir();
        }else{
            addFirstDataHeir();
        }
        loadList();
    }


    private void addFirstDataHeir(){
        heirListRegulers = new ArrayList<>();
        HeirListReguler heirListReguler = new HeirListReguler();
        heirListReguler.setPhoneNumber("");
        heirListReguler.setHeirName("");
        heirListReguler.setRelationshipCode("");
        heirListReguler.setRelationshipDetail("");
        heirListRegulers.add(heirListReguler);
    }

    private void addDataHeir(){
        HeirListReguler heirListReguler = new HeirListReguler();
        heirListReguler.setPhoneNumber("");
        heirListReguler.setHeirName("");
        heirListReguler.setRelationshipCode("");
        heirListReguler.setRelationshipDetail("");
        heirListRegulers.add(heirListReguler);
    }


    private void updateListPackage(){
        if(adapter.getList() != null && adapter.getList().size() != 0){
            heirListRegulers = new ArrayList<>();
            for(int i = 0; i < adapter.getList().size(); i++){
                HeirListReguler heirListReguler = new HeirListReguler();
                heirListReguler.setHeirName(adapter.getList().get(i).getHeirName());
                heirListReguler.setRelationshipCode(adapter.getList().get(i).getRelationshipCode());
                heirListReguler.setRelationshipDetail(adapter.getList().get(i).getRelationshipDetail());
                heirListReguler.setPhoneNumber(adapter.getList().get(i).getPhoneNumber());
                heirListRegulers.add(heirListReguler);
            }
        }
    }


    private void loadList(){
        adapter = new HeirDataAdapter(getActivity(), heirListRegulers, kycLookupRelationship, this);
        rv.setAdapter(adapter);
    }


    @OnClick(R.id.btnNext)
    void btnNext(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            setValueReksaDana();
            if(typeAccount.equals(Constant.INVEST_TYPE_REGULER)){
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_INVEST_REGULER, acountRequest));
            }else if(typeAccount.equals(Constant.INVEST_TYPE_LUMPSUM)){
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_INVEST_LUMPSUM, acountRequest));
            }
        }
    }


    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }


    public void dismissProgressBar() {
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError() {
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }


    @Override
    public void removeHeir(int index) {
        if(adapter.getList() != null && adapter.getList().size() != 0){
            heirListRegulers = new ArrayList<>();
            for(int i = 0; i < adapter.getList().size(); i++){
                HeirListReguler heirListReguler = new HeirListReguler();
                heirListReguler.setHeirName(adapter.getList().get(i).getHeirName());
                heirListReguler.setRelationshipCode(adapter.getList().get(i).getRelationshipCode());
                heirListReguler.setRelationshipDetail(adapter.getList().get(i).getRelationshipDetail());
                heirListReguler.setPhoneNumber(adapter.getList().get(i).getPhoneNumber());
                heirListRegulers.add(heirListReguler);
            }
            heirListRegulers.remove(index);
            loadList();
        }
    }



    public boolean validate(){
        boolean valid = true;
        if(adapter.getList() != null && adapter.getList().size() != 0){
            for(int i = 0; i < adapter.getList().size(); i++){

                if(adapter.getList().get(i).getRelationshipCode().equals("")){
                    valid = false;
                    Toast.makeText(getContext(), "Hubungan dengan ahli waris tidak boleh kosong!", Toast.LENGTH_LONG).show();
                    break;
                }

                if(adapter.getList().get(i).getRelationshipCode().equals("oth")){
                    if(adapter.getList().get(i).getRelationshipDetail().equals("")){
                        valid = false;
                        Toast.makeText(getContext(), "Hubungan dengan ahli waris tidak boleh kosong!", Toast.LENGTH_LONG).show();
                        break;
                    }else{
                        if(adapter.getList().get(i).getRelationshipDetail().length() < 4){
                            valid = false;
                            Toast.makeText(getContext(), "Hubungan dengan ahli waris tidak sesuai!", Toast.LENGTH_LONG).show();
                            break;
                        }
                    }
                }

                if(adapter.getList().get(i).getPhoneNumber().equals("")){
                    valid = false;
                    Toast.makeText(getContext(), "Phone number tidak boleh kosong!", Toast.LENGTH_LONG).show();
                    break;
                }

                if(adapter.getList().get(i).getPhoneNumber().length() < 3){
                    valid = false;
                    Toast.makeText(getContext(), "Phone number tidak sesuai!", Toast.LENGTH_LONG).show();
                    break;
                }

                if(adapter.getList().get(i).getHeirName().equals("")){
                    valid = false;
                    Toast.makeText(getContext(), "Nama ahli waris tidak boleh kosong!", Toast.LENGTH_LONG).show();
                    break;
                }

                if(adapter.getList().get(i).getHeirName().length() < 3){
                    valid = false;
                    Toast.makeText(getContext(), "Nama ahli waris tidak sesuai!", Toast.LENGTH_LONG).show();
                    break;
                }

                if(cbAccept.isChecked()){
                }else{
                   valid = false;
                    Toast.makeText(getContext(), "Anda belum menyetujui ahli waris!", Toast.LENGTH_LONG).show();
                    break;
                }

            }
        }else{
            valid = false;
            Toast.makeText(getContext(), "Data tidak boleh kosong!", Toast.LENGTH_LONG).show();
        }
        return  valid;
    }

    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }

    @Override
    public void nextWithoutValidation(){
    }

    @Override
    public void saveDataKycWithBackpress(){
    }

    private void setValueReksaDana(){
        if(typeAccount.equals(Constant.INVEST_TYPE_REGULER)){
            List<HeirList> heirLists = new ArrayList<>();
            for(int i = 0; i < heirListRegulers.size(); i++){
                HeirList heirList = new HeirList();
                heirList.setHeirName(heirListRegulers.get(i).getHeirName());
                heirList.setHeirMobileNumber(heirListRegulers.get(i).getPhoneNumber());
                heirList.setHeirRelationship(heirListRegulers.get(i).getRelationshipCode());
                if(heirListRegulers.get(i).getRelationshipCode().equalsIgnoreCase("oth")){
                    heirList.setHeirRelationshipDetail(heirListRegulers.get(i).getRelationshipDetail());
                }
                heirLists.add(heirList);
            }
            acountRequest.setHeirList(heirLists);
        }else if(typeAccount.equals(Constant.INVEST_TYPE_LUMPSUM)){
            List<HeirList> heirLists = new ArrayList<>();
            for(int i = 0; i < heirListRegulers.size(); i++){
                HeirList heirList = new HeirList();
                heirList.setHeirName(heirListRegulers.get(i).getHeirName());
                heirList.setHeirMobileNumber(heirListRegulers.get(i).getPhoneNumber());
                heirList.setHeirRelationship(heirListRegulers.get(i).getRelationshipCode());
                if(heirListRegulers.get(i).getRelationshipCode().equalsIgnoreCase("oth")){
                    heirList.setHeirRelationshipDetail(heirListRegulers.get(i).getRelationshipDetail());
                }
                heirLists.add(heirList);
            }
            acountRequest.setHeirList(heirLists);
        }

    }


    @Override
    public void addAccountBackpress(){
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.TO_PAGE_BEFORE_HEIR, null));
    }




}
