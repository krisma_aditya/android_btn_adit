package com.danareksa.investasik.ui.fragments.portfolio;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.InvestmentAccountGroup;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.adapters.pager.PortfolioPilihInvestorPageAdapter;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.ui.NonSwipeableViewPager;
import com.danareksa.investasik.util.ui.WrapContentHeightViewPager;

import butterknife.Bind;
import butterknife.OnClick;
import icepick.State;

/**
 * Created by asep.surahman on 23/05/2018.
 */

public class ListCategoryPortfolioFragment extends BaseFragment {

    @Bind(R.id.lnProgressBar)
    LinearLayout lnProgressBar;
    @Bind(R.id.lnDismissBar)
    RelativeLayout lnDismissBar;
    @Bind(R.id.pbLoading)
    ProgressBar pbLoading;
    @Bind(R.id.lnConnectionError)
    LinearLayout lnConnectionError;

    @Bind(R.id.pager)
    NonSwipeableViewPager pager;

    @Bind(R.id.bLumpsum)
    Button bLumpsum;

    @Bind(R.id.bReguler)
    Button bReguler;

    private PortfolioPilihInvestorPageAdapter pagerAdapter;
    ListCategoryPortfolioPresenter presenter;

    @State
    int pageNumber = 0;
    int status = 0;
    InvestmentAccountGroup accountGroup;


    public static final String TAG = ListCategoryPortfolioFragment.class.getSimpleName();

    public static void showFragment(BaseActivity sourceActivity){
        if (!sourceActivity.isFragmentNotNull(TAG)) {
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new ListCategoryPortfolioFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    @Override
    protected int getLayout() {
        return R.layout.f_portfolio_pilih_investor;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        accountGroup = new InvestmentAccountGroup();
        presenter = new ListCategoryPortfolioPresenter(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.getInvestmentAccountGroup();
        status = 0;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @OnClick(R.id.bLumpsum)
    void bLumpsum() {
        if (pageNumber > 0) {
            pageNumber--;
            pager.setCurrentItem(pageNumber);

            if (status == 0){
                bLumpsum.setBackgroundResource(R.drawable.left_bg_round_white);
                bReguler.setBackgroundResource(R.drawable.right_bg_round_blue);
                bLumpsum.setTextColor(getResources().getColor(R.color.colorPrimary));
                bReguler.setTextColor(getResources().getColor(R.color.white));
                status = 1;
            } else if (status == 1) {
                bLumpsum.setBackgroundResource(R.drawable.left_bg_round_blue);
                bReguler.setBackgroundResource(R.drawable.right_bg_round_white);
                bLumpsum.setTextColor(getResources().getColor(R.color.white));
                bReguler.setTextColor(getResources().getColor(R.color.colorPrimary));
                status = 0;
            }
        }
    }

    @OnClick(R.id.bReguler)
    void bReguler(){
        if (pageNumber < pagerAdapter.getCount() - 1){
            pageNumber++;
            pager.setCurrentItem(pageNumber);

            if (status == 0) {
                bLumpsum.setBackgroundResource(R.drawable.left_bg_round_white);
                bReguler.setBackgroundResource(R.drawable.right_bg_round_blue);
                bLumpsum.setTextColor(getResources().getColor(R.color.colorPrimary));
                bReguler.setTextColor(getResources().getColor(R.color.white));
                status = 1;
            } else if (status == 1) {
                bLumpsum.setBackgroundResource(R.drawable.left_bg_round_blue);
                bReguler.setBackgroundResource(R.drawable.right_bg_round_white);
                bLumpsum.setTextColor(getResources().getColor(R.color.white));
                bReguler.setTextColor(getResources().getColor(R.color.colorPrimary));
                status = 0;
            }
        }


    }


    public void setupViewPager(){
        pagerAdapter = new PortfolioPilihInvestorPageAdapter(this, getChildFragmentManager(), accountGroup);
        pager.setAdapter(pagerAdapter);
        pager.addOnPageChangeListener(new WrapContentHeightViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position){
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    public void showProgressBar(){
        pbLoading.setVisibility(View.VISIBLE);
        lnConnectionError.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
    }

    public void dismissProgressBar(){
        lnProgressBar.setVisibility(View.GONE);
        lnDismissBar.setVisibility(View.VISIBLE);
    }

    public void connectionError(){
        lnProgressBar.setVisibility(View.VISIBLE);
        lnDismissBar.setVisibility(View.GONE);
        pbLoading.setVisibility(View.GONE);
        lnConnectionError.setVisibility(View.VISIBLE);
    }



}
