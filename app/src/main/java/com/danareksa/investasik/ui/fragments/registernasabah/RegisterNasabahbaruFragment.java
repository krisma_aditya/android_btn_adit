package com.danareksa.investasik.ui.fragments.registernasabah;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.RegisterNewCustomer;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.ui.activities.RegisterNasabahSyaratKetenActivity;
import com.danareksa.investasik.ui.fragments.BaseFragment;
import com.danareksa.investasik.util.DateUtil;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by asep.surahman on 08/05/2018.
 */

public class RegisterNasabahbaruFragment extends BaseFragment {

    public static final String TAG = RegisterNasabahbaruFragment.class.getSimpleName();
    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etBirtDate)
    EditText etBirtDate;
    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etFirstName)
    EditText etFirstName;
    @Bind(R.id.etMiddleName)
    EditText etMiddleName;
    @Bind(R.id.etLastName)
    EditText etLastName;
    @Email
    @NotEmpty(messageResId = R.string.rules_no_empty_email)
    @Bind(R.id.etEmail)
    EditText etEmail;
    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Bind(R.id.etMobileNumber)
    EditText etMobileNumber;
    //^(?=.*[0-9])(?=.*[!.,;*@#$%^&+=])(?=\S+$).{8,}$
    @NotEmpty(messageResId = R.string.rules_no_empty)
    @Pattern(regex = "^(?=.*[0-9])(?=.*?[.:;#?!@$%^&*-])(?=\\S+$).{8,}$", messageResId = R.string.rules_password)
    @Password
    @Bind(R.id.etPassword)
    EditText etPassword;
    @ConfirmPassword
    @Bind(R.id.etConfirmPassword)
    EditText etConfirmPassword;
    @Bind(R.id.tvPwdHint)
    TextView tvPwdHint;

    String birthDate = "";


    public static void showFragment(BaseActivity sourceActivity){
        if(!sourceActivity.isFragmentNotNull(TAG)){
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new RegisterNasabahbaruFragment(), TAG);
            fragmentTransaction.commit();
        }
    }

    public static Fragment getFragment(){
        Fragment f = new RegisterNasabahbaruFragment();
        return f;
    }

    @Override
    protected int getLayout(){
        return R.layout.f_register_nasabah_baru_form;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvPwdHint.setHint(Html.fromHtml(getString(R.string.signup_password_hint)));
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @OnClick(R.id.etBirtDate)
    void etBirtDate(){
        DateUtil.showDatePickerDialog(getActivity().getSupportFragmentManager(), new DateUtil.DateDialogPickerListener() {
            @Override
            public void onDatePick(String date, String month, String year){
                etBirtDate.setText(date + " " + DateUtil.getMonthString(month) + " " + year);
                birthDate = year + "-" + DateUtil.getMonthNumber(month) + "-" + DateUtil.getDateNumberString(date);
                etBirtDate.setError(null);

            }
        }, DateUtil.DatePickerFragment.TYPE_BIRTH_DATE);
    }


    @OnClick(R.id.bStart)
    void bStart(){

        if(etEmail.length() >0){
            String lowercase = etEmail.getText()+"";
            etEmail.setText(lowercase.toLowerCase());
        }

        if(!validate()){
            onSignupFailed();
            return;
        }else{
            validator.validate();
        }

    }

    public void onSignupFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    public boolean validate(){

        boolean valid = true;
        String firstName      = etFirstName.getText().toString();
        String email          = etEmail.getText().toString();
        String mobileNumber   = etMobileNumber.getText().toString();
        String password       = etPassword.getText().toString();
        String confirmpass    = etConfirmPassword.getText().toString();

        if(firstName.isEmpty() && firstName.length() <= 0){
            etFirstName.setError("Mohon isi kolom ini");
            valid = false;
        }else{
            etFirstName.setError(null);
        }

        if(firstName.length() < 3){
            etFirstName.setError("Minimal 3 karakter");
            valid = false;
        }else{
            etFirstName.setError(null);
        }

        if(mobileNumber.isEmpty()){
            etMobileNumber.setError("Mohon isi kolom ini");
            valid = false;
        }else{
            etMobileNumber.setError(null);
        }

        if(birthDate.isEmpty()){
            etBirtDate.setError("Mohon isi kolom ini");
            valid = false;
        }else{
            etBirtDate.setError(null);
        }

        if(email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            etEmail.setError("Masukkan email Anda dengan benar");
            valid = false;
        }

        String match = "^(?=.*[0-9])(?=.*?[.:;#?!@$%^&*-])(?=\\S+$).{8,}$";
        if(password.isEmpty() || !password.matches(match)){
            etPassword.setError("Minimal 8 karakter terdiri dari kombinasi alphabet, angka dan spesial karakter");
            valid = false;
        }


        String confirmpassmatch = "^(?=.*[0-9])(?=.*?[.:;#?!@$%^&*-])(?=\\S+$).{8,}$";
        if(confirmpass.isEmpty() || !confirmpass.matches(confirmpassmatch)){
            etConfirmPassword.setError("Minimal 8 karakter terdiri dari kombinasi alphabet, angka dan spesial karakter");
            valid = false;
        }


        if(!password.equals(confirmpass)){
            etConfirmPassword.setError("Masukkan konfirmasi password dengan benar");
            valid = false;
        }

        return valid;
    }



    @Override
    public void onValidationSucceeded(){
        super.onValidationSucceeded();
        RegisterNasabahSyaratKetenActivity.startActivity((BaseActivity) getActivity(), "baru", getRegisterNewCustomerModel(), etEmail.getText().toString());
    }


    private RegisterNewCustomer getRegisterNewCustomerModel(){
        RegisterNewCustomer registerNewCustomer = new RegisterNewCustomer();
        registerNewCustomer.setFirstName(etFirstName.getText().toString());
        registerNewCustomer.setMiddleName(etMiddleName.getText().toString());
        registerNewCustomer.setLastName(etLastName.getText().toString());
        registerNewCustomer.setEmail(etEmail.getText().toString());
        registerNewCustomer.setBirthDate(birthDate);
        registerNewCustomer.setMobileNumber(etMobileNumber.getText().toString());
        registerNewCustomer.setPassword(etPassword.getText().toString());
        return registerNewCustomer;
    }


    //cobain besok
    private boolean _hasLoadedOnce= false; // your boolean field
    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);


        if (this.isVisible()){
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                //new NetCheck().execute(); metod
                System.out.println("i'm here..");
                _hasLoadedOnce = true;
            }
        }
    }



}
