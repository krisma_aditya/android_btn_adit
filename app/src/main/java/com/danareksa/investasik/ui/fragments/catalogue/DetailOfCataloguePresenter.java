package com.danareksa.investasik.ui.fragments.catalogue;

import com.danareksa.investasik.data.api.beans.ProductList;
import com.danareksa.investasik.data.api.responses.PackageResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by fajarfatur on 2/2/16.
 */
public class DetailOfCataloguePresenter {

    private DetailOfCatalogueFragment fragment;

    public DetailOfCataloguePresenter(DetailOfCatalogueFragment fragment) {
        this.fragment = fragment;
    }

    void packageDetail(final ProductList packages) {
        fragment.showProgressBar();
        fragment.getApi().packageDetail(packages.getId(), PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<PackageResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(PackageResponse response) {
                        if (response.getCode() == 1) {
                            fragment.packages = response.getData();
                            fragment.packages.setId(packages.getId());
                            fragment.setPackagesDataToView();
                            fragment.setupViewPager();
                            fragment.dismissProgressBar();
                        } else {
                            fragment.showFailedDialog(response.getInfo());
                        }

                    }
                });
    }
}
