package com.danareksa.investasik.ui.fragments.kycnew;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.danareksa.investasik.BuildConfig;
import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.InviseeService;
import com.danareksa.investasik.data.api.beans.ImageIdUpload;
import com.danareksa.investasik.data.api.beans.ImageSelfieUpload;
import com.danareksa.investasik.data.api.beans.ImageTaxIdUpload;
import com.danareksa.investasik.data.api.requests.KycDataRequest;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;
import com.danareksa.investasik.util.eventBus.RxBusObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import butterknife.Bind;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;

import static android.app.Activity.RESULT_OK;

/**
 * Created by asep.surahman on 15/05/2018.
 */

@RuntimePermissions
public class Kyc1DocumentDataFragment extends KycBaseNew {

    public static final String TAG = Kyc1DocumentDataFragment.class.getSimpleName();

    public static final String KYC_DATA_REQUEST = "kycDataRequest";

    @Bind(R.id.ivSelfiePhoto)
    CircleImageView ivSelfiePhoto;
    @Bind(R.id.ivIdPhoto)
    CircleImageView ivIdPhoto;
    @Bind(R.id.ivNpwpPhoto)
    CircleImageView ivNpwpPhoto;
    @Bind(R.id.tvFotoSelfie)
    TextView tvFotoSelfie;

    private int choosenTask = 0;
    private final int SELECT_PHOTO_ID = 0;
    private final int REQUEST_CAMERA_ID = 2;
    private final int SELECT_PHOTO_SELFIE = 4;
    private final int REQUEST_CAMERA_SELFIE = 6;
    private final int SELECT_PHOTO_NPWP = 8;
    private final int REQUEST_CAMERA_NPWP = 10;
    public static final String TYPE_REQUEST_ID = "T_ID";
    public static final String TYPE_REQUEST_SELFIE = "T_SELFIE";
    public static final String TYPE_REQUEST_NPWP = "T_NPWP";

    private File tempFile;
    private Uri picUriId, picUriSelfie, picUriTax;
    private String IdPhoto = "", fileName = "", idSelfie = "", fileNameSelfie = "", idNpwp = "", fileNameNpwp = "";
    boolean validImageInitial = false;
    public KycDataRequest kycDataRequest;

    public static void showFragment(BaseActivity sourceActivity){
        if(!sourceActivity.isFragmentNotNull(TAG)){
            FragmentTransaction fragmentTransaction = sourceActivity.getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.container, new Kyc1DocumentDataFragment(), TAG);
            fragmentTransaction.commit();
        }
    }


    public static Fragment getFragment(KycDataRequest kycDataRequest){
        Bundle bundle = new Bundle();
        bundle.putSerializable(KYC_DATA_REQUEST, kycDataRequest);
        Kyc1DocumentDataFragment fragment = new Kyc1DocumentDataFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayout() {
        return R.layout.f_register_documen;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        kycDataRequest = (KycDataRequest) getArguments().getSerializable(KYC_DATA_REQUEST);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        init();
        loadIdImage();
        loadSelfieImage();
        loadNpwpImage();
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    private void init(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN")) {
            tvFotoSelfie.setText("Foto selfie bersama KTP/Paspor Anda");
        }
    }



    /*===================GET ID PHOTO=========================*/
    @OnClick(R.id.ivIdPhoto)
    void ivIdPhoto(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            new MaterialDialog.Builder(getActivity())
                    .title("Change Picture")
                    .items(R.array.change_picture)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            if (which == 0){
                                choosenTask = REQUEST_CAMERA_ID;
                                Kyc1DocumentDataFragmentPermissionsDispatcher.startCameraIdWithPermissionCheck(Kyc1DocumentDataFragment.this);
                            } else {
                                choosenTask = SELECT_PHOTO_ID;
                                Kyc1DocumentDataFragmentPermissionsDispatcher.startGalleryIdWithPermissionCheck(Kyc1DocumentDataFragment.this);
                            }
                        }
                    })
                    .show();
        }
    }


    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void startGalleryId(){
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_PHOTO_ID);
    }

    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void startCameraId(){
        tempFile = createTemporaryFile(getContext(), "id_pic", ".jpg");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        picUriId = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", tempFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUriId);
        startActivityForResult(intent, REQUEST_CAMERA_ID);
    }
    /*======================END ID PHOTO==========================*/


    /*===================GET SELFIE PHOTO=========================*/
    @OnClick(R.id.ivSelfiePhoto)
    void ivSelfiePhoto(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            new MaterialDialog.Builder(getActivity())
                    .title("Change Picture")
                    .items(R.array.change_picture)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            if (which == 0) {
                                choosenTask = REQUEST_CAMERA_SELFIE;
                                Kyc1DocumentDataFragmentPermissionsDispatcher.startCameraSelfieWithPermissionCheck(Kyc1DocumentDataFragment.this);
                            } else {
                                choosenTask = SELECT_PHOTO_SELFIE;
                                Kyc1DocumentDataFragmentPermissionsDispatcher.startGallerySelfieWithPermissionCheck(Kyc1DocumentDataFragment.this);
                            }
                        }
                    })
                    .show();
        }
    }


    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void startGallerySelfie() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_PHOTO_SELFIE);
    }

    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void startCameraSelfie(){
        tempFile = createTemporaryFile(getContext(), "selfie_pic", ".jpg");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        picUriSelfie = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", tempFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUriSelfie);
        startActivityForResult(intent, REQUEST_CAMERA_SELFIE);
    }
    /*======================END GET SELFIE=======================*/


    /*===================GET NPWP PHOTO=========================*/
    @OnClick(R.id.ivNpwpPhoto)
    void ivNpwpPhoto(){
        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
            new MaterialDialog.Builder(getActivity())
                    .title("Change Picture")
                    .items(R.array.change_picture)
                    .itemsCallback(new MaterialDialog.ListCallback() {
                        @Override
                        public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            if (which == 0) {
                                choosenTask = REQUEST_CAMERA_NPWP;
                                Kyc1DocumentDataFragmentPermissionsDispatcher.startCameraNpwpWithPermissionCheck(Kyc1DocumentDataFragment.this);
                            } else {
                                choosenTask = SELECT_PHOTO_NPWP;
                                Kyc1DocumentDataFragmentPermissionsDispatcher.startGalleryNpwpWithPermissionCheck(Kyc1DocumentDataFragment.this);
                            }
                        }
                    })
                    .show();
        }
    }


    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void startGalleryNpwp() {
        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_PHOTO_NPWP);
    }

    @NeedsPermission({Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    public void startCameraNpwp(){
        tempFile = createTemporaryFile(getContext(), "npwp_pic", ".jpg");
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        picUriTax = FileProvider.getUriForFile(getContext(), BuildConfig.APPLICATION_ID + ".provider", tempFile);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUriTax);
        startActivityForResult(intent, REQUEST_CAMERA_NPWP);
    }
    /*=========================END=========================*/


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Kyc1DocumentDataFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent){
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode){
            case SELECT_PHOTO_ID:
                if (resultCode == RESULT_OK){

                    Uri selectedImage = imageReturnedIntent.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    File file = new File(picturePath);
                    long length = file.length() / 1024; // Size in KB


                    tempFile = file;
                    onCaptureImageResult(TYPE_REQUEST_ID);

                }
                break;
            case REQUEST_CAMERA_ID:
                if (resultCode == Activity.RESULT_OK) {
                    onCaptureImageResult(TYPE_REQUEST_ID);
                }
                break;
            case SELECT_PHOTO_SELFIE:
                if (resultCode == RESULT_OK) {

                    Uri selectedImage = imageReturnedIntent.getData();

                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    File file = new File(picturePath);
                    long length = file.length() / 1024; // Size in KB


                    tempFile = file;
                    onCaptureImageResult(TYPE_REQUEST_SELFIE);

                }
                break;
            case REQUEST_CAMERA_SELFIE:
                if (resultCode == Activity.RESULT_OK) {
                    onCaptureImageResult(TYPE_REQUEST_SELFIE);
                }
                break;
            case SELECT_PHOTO_NPWP:
                if (resultCode == RESULT_OK){
                    Uri selectedImage = imageReturnedIntent.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    File file = new File(picturePath);
                    long length = file.length() / 1024; // Size in KB


                    tempFile = file;
                    onCaptureImageResult(TYPE_REQUEST_NPWP);

                }
                break;
            case REQUEST_CAMERA_NPWP:
                if (resultCode == Activity.RESULT_OK) {
                    onCaptureImageResult(TYPE_REQUEST_NPWP);
                }
                break;

        }
    }



    void onCaptureImageResult(String type){

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(tempFile.getPath(), options);
        final int REQUIRED_SIZE = 310;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;

        // Load file with option parameter, then compress it
        Bitmap b = BitmapFactory.decodeFile(tempFile.getPath(), options);
        Matrix matrix = new Matrix();

        try {
            ExifInterface exif = null;
            exif = new ExifInterface(tempFile.getPath());
            String orientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            if (orientation.equals(ExifInterface.ORIENTATION_NORMAL)) {
                // Do nothing. The original image is fine.
            } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_90 + "")) {
                matrix.postRotate(90);
            } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_180 + "")) {
                matrix.postRotate(180);
            } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_270 + "")) {
                matrix.postRotate(270);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Bitmap resizedBitmap = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
        resizedBitmap.compress(Bitmap.CompressFormat.JPEG, 70, bytes);

        byte[] byteArray = bytes.toByteArray();
        // get the base 64 string

        if(type.equals(TYPE_REQUEST_ID)){
            fileName = tempFile.getName();
            IdPhoto = Base64.encodeToString(byteArray, Base64.DEFAULT);
            ivIdPhoto.setImageBitmap(resizedBitmap);
            PrefHelper.setString(PrefKey.NAME_PHOTO_KTP, fileName);
            PrefHelper.setString(PrefKey.ID_PHOTO_KTP, IdPhoto);
        }else if(type.equals(TYPE_REQUEST_SELFIE)){
            fileNameSelfie = tempFile.getName();
            idSelfie = Base64.encodeToString(byteArray, Base64.DEFAULT);
            ivSelfiePhoto.setImageBitmap(resizedBitmap);
            PrefHelper.setString(PrefKey.NAME_PHOTO_SELFIE, fileNameSelfie);
            PrefHelper.setString(PrefKey.ID_PHOTO_SELFIE, idSelfie);
        }else if(type.equals(TYPE_REQUEST_NPWP)){
            fileNameNpwp = tempFile.getName();
            idNpwp = Base64.encodeToString(byteArray, Base64.DEFAULT);
            ivNpwpPhoto.setImageBitmap(resizedBitmap);
            PrefHelper.setString(PrefKey.NAME_PHOTO_NPWP, fileNameNpwp);
            PrefHelper.setString(PrefKey.ID_PHOTO_NPWP, idNpwp);
        }
    }



    @Override
    public void saveDataKycWithBackpress(){
        if(!validate()){
            onValidFailed();
            return;
        }else{
            if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("ACT") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("REG")) {
                setValueImageId();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.SAVE_KYC_DATA, request));
            }else{
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.FINISH_STEP, null));
            }
        }
    }

    @Override
    public void previewsWithoutValidation() {
        getBus().send(new RxBusObject(RxBusObject.RxBusKey.BACK_TO_PAGE, null));
    }


    @Override
    public void nextWithoutValidation(){

        if(PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("PEN") || PrefHelper.getString(PrefKey.CUSTOMER_STATUS).equals("VER")) {
            getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
        }else{
            if(!validate()){
                onValidFailed();
            }else{
                setValueImageId();
                getBus().send(new RxBusObject(RxBusObject.RxBusKey.NEXT_FORM, null));
            }
            return;
        }

    }


    public void onValidFailed(){
        Toast.makeText(getContext(), "Silahkan lengkapi data yang dibutuhkan", Toast.LENGTH_LONG).show();
    }


    private void setValueImageId(){

        if(request.getIdImageKey() != null){
            if(!request.getIdImageKey().equals("")){
                //if photo already exis
                if(!fileName.equals("") && !IdPhoto.equals("")){
                    uploadDataId();
                }

            }else{
                uploadDataId();
            }
        }else{
            uploadDataId();
        }


        if(request.getSefieImageKey() != null){
            if(!request.getSefieImageKey().equals("")){
                //if photo already exis
                if(!fileNameSelfie.equals("") && !idSelfie.equals("")){
                    uploadDataSelfie();
                }
            }else{
                uploadDataSelfie();
            }
        }else{
            uploadDataSelfie();
        }


        if(request.getTaxIdImageKey() != null){
            if(!request.getTaxIdImageKey().equals("")){
                //if photo already exis
                if(!fileNameNpwp.equals("") && !idNpwp.equals("")){
                    uploadDataNpwp();
                }
            }else{
                uploadDataNpwp();
            }
        }else{
            uploadDataNpwp();
        }
    }


    public void loadIdImage(){
        try {

            if(request.getIdImageKey() !=  null){
                if(!request.getIdImageKey().equals("")){
                    validImageInitial = true;

                    Glide.with(getContext()).load(InviseeService.IMAGE_CUSTOMER_DOWNLOAD_URL + request.getIdImageKey() + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                            .override(400, 400)
                            .error(R.color.grey_200)
                            .fitCenter()
                            .into(ivIdPhoto);

                }
            }else{
                ivIdPhoto.setImageResource(R.drawable.ic_photo_ktp);
            }
        } catch (Exception e) {
            ivIdPhoto.setImageResource(R.drawable.ic_photo_ktp);
        }
    }


    public void loadSelfieImage(){
        try {
            if(request.getSefieImageKey() != null){
                if(!request.getSefieImageKey().equals("")){
                    validImageInitial = true;

                    Glide.with(getContext()).load(InviseeService.IMAGE_CUSTOMER_DOWNLOAD_URL + request.getSefieImageKey()+ "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                            .override(400, 400)
                            .error(R.color.grey_200)
                            .fitCenter()
                            .into(ivSelfiePhoto);

                }else{
                    ivSelfiePhoto.setImageResource(R.drawable.ic_photo_id);
                }
            }else{
                ivSelfiePhoto.setImageResource(R.drawable.ic_photo_id);
            }
        } catch (Exception e) {
            ivSelfiePhoto.setImageResource(R.drawable.ic_photo_id);
        }
    }

    public void loadNpwpImage(){
        try {
            if(request.getTaxIdImageKey() != null){
                if(!request.getTaxIdImageKey().equals("")){

                    Glide.with(getContext()).load(InviseeService.IMAGE_CUSTOMER_DOWNLOAD_URL + request.getTaxIdImageKey() + "&token=" + PrefHelper.getString(PrefKey.TOKEN))
                            .override(400, 400)
                            .error(R.color.grey_200)
                            .fitCenter()
                            .into(ivNpwpPhoto);

                }else{
                    ivNpwpPhoto.setImageResource(R.drawable.ic_photo_npwp);
                }
            }else{
                ivNpwpPhoto.setImageResource(R.drawable.ic_photo_npwp);
            }

        }catch(Exception e){
            ivNpwpPhoto.setImageResource(R.drawable.ic_photo_npwp);
        }

    }


    public boolean validate(){

        boolean valid = true;
        if(validImageInitial == true){
            valid = true;
        }else{

            if(IdPhoto.equals("")){
                Toast.makeText(getActivity(), "Masukkan ID foto", Toast.LENGTH_SHORT).show();
                valid = false;
            }

            if(idSelfie.equals("")){
                Toast.makeText(getActivity(), "Masukkan foto selfi", Toast.LENGTH_SHORT).show();
                valid = false;
            }

        }

        return valid;
    }

    private void uploadDataId(){
        ImageIdUpload imageIdUpload = new ImageIdUpload();
        if(!fileName.equals("") && !IdPhoto.equals("")){
            imageIdUpload.setFile_name(fileName);
            imageIdUpload.setContent(IdPhoto);
        }else{
            if(!PrefHelper.getString(PrefKey.NAME_PHOTO_KTP).equals("")){
                imageIdUpload.setFile_name(PrefHelper.getString(PrefKey.NAME_PHOTO_KTP));
            }
            if(!PrefHelper.getString(PrefKey.ID_PHOTO_KTP).equals("")){
                imageIdUpload.setContent(PrefHelper.getString(PrefKey.ID_PHOTO_KTP));
            }
        }

        request.setIdImage(imageIdUpload);
    }

    private void uploadDataSelfie(){
        ImageSelfieUpload imageSelfieUpload = new ImageSelfieUpload();
        if(!fileNameSelfie.equals("") && !idSelfie.equals("")){
            imageSelfieUpload.setFile_name(fileNameSelfie);
            imageSelfieUpload.setContent(idSelfie);
        }else{
            if(!PrefHelper.getString(PrefKey.NAME_PHOTO_SELFIE).equals("")){
                imageSelfieUpload.setFile_name(PrefHelper.getString(PrefKey.NAME_PHOTO_SELFIE));
            }
            if(!PrefHelper.getString(PrefKey.ID_PHOTO_SELFIE).equals("")){
                imageSelfieUpload.setContent(PrefHelper.getString(PrefKey.ID_PHOTO_SELFIE));
            }
        }
        request.setSelfieImage(imageSelfieUpload);
    }

    private void uploadDataNpwp(){
        ImageTaxIdUpload imageTaxIdUpload = new ImageTaxIdUpload();

        if(!fileNameNpwp.equals("") && !idNpwp.equals("")){
            imageTaxIdUpload.setFile_name(fileNameNpwp);
            imageTaxIdUpload.setContent(idNpwp);
        }else{
            if(!PrefHelper.getString(PrefKey.NAME_PHOTO_NPWP).equals("")){
                imageTaxIdUpload.setFile_name(PrefHelper.getString(PrefKey.NAME_PHOTO_NPWP));
            }
            if(!PrefHelper.getString(PrefKey.ID_PHOTO_NPWP).equals("")){
                imageTaxIdUpload.setContent(PrefHelper.getString(PrefKey.ID_PHOTO_NPWP));
            }
        }

        request.setTaxIdImage(imageTaxIdUpload);
    }


    public static File createTemporaryFile(Context context, String folder_name, String ext){
        try {
            File folder = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), folder_name);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            return File.createTempFile(""+System.currentTimeMillis(), ext, folder);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    void writeFile(InputStream in, File file) {
        OutputStream out = null;
        try {
            out = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while((len=in.read(buf))>0){
                out.write(buf,0,len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if ( out != null ) {
                    out.close();
                }
                in.close();
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
    }



    @Override
    public void previewsWhileError(){

    }



}