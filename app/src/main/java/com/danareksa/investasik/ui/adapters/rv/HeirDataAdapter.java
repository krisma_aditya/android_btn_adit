package com.danareksa.investasik.ui.adapters.rv;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.danareksa.investasik.R;
import com.danareksa.investasik.data.api.beans.HeirListReguler;
import com.danareksa.investasik.data.api.beans.KycLookup;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by asep.surahman on 06/08/2018.
 */

public class HeirDataAdapter extends RecyclerView.Adapter<HeirDataAdapter.HeirDataHolder> {


    List<HeirListReguler> heirListRegulers;
    private HeirListReguler heirListReguler;
    private List<HeirDataAdapter.HeirDataHolder> holderList;
    List<KycLookup> kycLookupRelationship;
    private Context context;
    private CallbackRemoveHeir mCallback;

    public HeirDataAdapter( Context context, List<HeirListReguler> heirListRegulers,  List<KycLookup> kycLookupRelationship, CallbackRemoveHeir mCallbacks) {
        this.heirListRegulers = heirListRegulers;
        this.holderList = new ArrayList<>();
        this.context = context;
        this.kycLookupRelationship = kycLookupRelationship;
        mCallback = mCallbacks;
    }


    @Override
    public HeirDataAdapter.HeirDataHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_heir, parent, false);
        return new HeirDataAdapter.HeirDataHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final HeirDataAdapter.HeirDataHolder holder, final int position){
        heirListReguler = heirListRegulers.get(position);
        holder.itemView.setTag(heirListReguler);

        setupSpinnerWithSpecificLookupSelection(holder.sHeirRelationship, kycLookupRelationship, String.valueOf(heirListReguler.getRelationshipCode()));
        if(!heirListReguler.getRelationshipCode().equals("")){
            holder.heirRelationshipCode = heirListReguler.getRelationshipCode();

            if(heirListReguler.getRelationshipCode().equalsIgnoreCase("oth")){
                holder.etHeirOther.setVisibility(View.VISIBLE);
                holder.etHeirOther.setText(heirListReguler.getRelationshipDetail());
            }else{
                holder.etHeirOther.setVisibility(View.GONE);
            }


        }else{
            holder.heirRelationshipCode = "";
        }


        if(!heirListReguler.getHeirName().equals("")){
            holder.etHeirName.setText(heirListReguler.getHeirName());
        }else{
            holder.etHeirName.setText("");
        }

        if(!heirListReguler.getPhoneNumber().equals("")){
            holder.etPhoneNumber.setText(heirListReguler.getPhoneNumber());
        }else{
            holder.etPhoneNumber.setText("");
        }

        holder.sHeirRelationship.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                holder.heirRelationshipCode = kycLookupRelationship.get(holder.sHeirRelationship.getSelectedItemPosition()).getCode();
                if(!holder.heirRelationshipCode.equals("")){
                    if(holder.heirRelationshipCode.equalsIgnoreCase("oth")){ //oth
                        holder.etHeirOther.setVisibility(View.VISIBLE);
                    }else{
                        holder.etHeirOther.setVisibility(View.GONE);
                    }
                }else{
                    holder.etHeirOther.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        holder.bRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeHeirList(position);
            }
        });

        holderList.add(holder);
    }


    protected void setupSpinner(Spinner s, List<KycLookup> kycLookupList){
        ArrayAdapter<KycLookup> spinnerArrayAdapter = new ArrayAdapter<>(context, R.layout.spinner, kycLookupList);
        s.setAdapter(spinnerArrayAdapter);
        s.setSelection(0, false);
    }

    protected void setupSpinnerWithSpecificLookupSelection(Spinner s, List<KycLookup> kycLookupList, String lookupCode) {
        setupSpinner(s, kycLookupList);
        int i = 0;
        for (KycLookup lookup : kycLookupList){
            if (lookup.getCode().equalsIgnoreCase(lookupCode)){
                s.setSelection(i, false);
                break;
            }
            i++;
        }
    }


    public List<HeirListReguler> getList(){

        for (int i = 0; i < heirListRegulers.size(); i++){

            HeirListReguler it = heirListRegulers.get(i);
            HeirDataAdapter.HeirDataHolder ho = holderList.get(i);

            it.setHeirName(String.valueOf(ho.etHeirName.getText()));
            it.setPhoneNumber(String.valueOf(ho.etPhoneNumber.getText()));

            if(!ho.heirRelationshipCode.equals("")){
                it.setRelationshipCode(ho.heirRelationshipCode);
                if(ho.heirRelationshipCode.equalsIgnoreCase("oth")){
                    it.setRelationshipDetail(String.valueOf(ho.etHeirOther.getText()));
                }

            }else{
                it.setRelationshipCode("");
            }



            heirListRegulers.set(i, it);
        }

        return heirListRegulers;
    }


    protected String getKycLookupCodeFromSelectedItemSpinner(Spinner s) {
        return ((KycLookup) s.getSelectedItem()).getCode();
    }


    @Override
    public int getItemCount(){
        return heirListRegulers != null ? heirListRegulers.size() : 0;
    }


    public static class HeirDataHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.etHeirName)
        EditText etHeirName;
        @Bind(R.id.etHeirOther)
        EditText etHeirOther;
        @Bind(R.id.etPhoneNumber)
        EditText etPhoneNumber;
        @Bind(R.id.sHeirRelationship)
        Spinner sHeirRelationship;
        @Bind(R.id.bRemove)
        Button bRemove;
        private String heirName;
        private String phoneNumber;
        private String heirRelationshipCode;
        private String heirRelationshipDetail;

        public HeirDataHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


    public interface CallbackRemoveHeir{
        void removeHeir(int index);
    }

    private void removeHeirList(int index){
        mCallback.removeHeir(index);
    }

}
