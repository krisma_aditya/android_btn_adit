package com.danareksa.investasik.ui.fragments.portfolio;

import com.danareksa.investasik.data.api.requests.InvestmentByProductTypeRequest;
import com.danareksa.investasik.data.api.responses.CartListResponse;
import com.danareksa.investasik.data.api.responses.InvestmentListByProductTypeResponse;
import com.danareksa.investasik.data.prefs.PrefHelper;
import com.danareksa.investasik.data.prefs.PrefKey;
import com.danareksa.investasik.ui.activities.BaseActivity;

import java.util.List;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by asep.surahman on 08/10/2018.
 */

public class ListPortfolioByProductTypePresenter {


    private ListPortfolioByProductTypeFragment fragment;

    public ListPortfolioByProductTypePresenter(ListPortfolioByProductTypeFragment fragment) {
        this.fragment = fragment;
    }


    public void cartList() {
        fragment.getApi().getCartList(PrefHelper.getString(PrefKey.TOKEN))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<List<CartListResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(List<CartListResponse> response) {
                        ((BaseActivity) fragment.getActivity()).setNotifCount(0);

                        if (response != null && response.size() > 0) {
                            ((BaseActivity) fragment.getActivity()).setNotifCount(response.size());
                            fragment.cartList = response;
                            for (int i = 0; i < response.size(); i++) {

                            }
                        } else {

                        }
                    }
                });
    }



    void getInvestmentListByProductType(Integer productType){

        InvestmentByProductTypeRequest request = new InvestmentByProductTypeRequest();
        request.setToken(PrefHelper.getString(PrefKey.TOKEN));
        request.setProductType(productType);

        fragment.showProgressBar();
        fragment.getApi().getInvestmentListByProductType(request)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Observer<InvestmentListByProductTypeResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        fragment.connectionError();
                    }

                    @Override
                    public void onNext(InvestmentListByProductTypeResponse investmentListByProductTypeResponse) {

                        fragment.dismissProgressBar();
                        if(investmentListByProductTypeResponse.getCode() == 1) {
                            if (investmentListByProductTypeResponse.getData().size() > 0) {
                                fragment.noPortfolio(false);
                                fragment.investmentListByProductType = investmentListByProductTypeResponse;
                                fragment.loadInvestmentList();
                            } else {
                                fragment.noPortfolio(true);
                            }
                        }else{
                            fragment.showDialogAfterSubmit(investmentListByProductTypeResponse.getInfo());
                        }

                    }
                });
    }


}
